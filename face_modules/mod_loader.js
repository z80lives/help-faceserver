
var confFile = "module.json"; //default configuration file to search for
var fs = require('fs'); //used by load_description(), get_directories()
var spawn = require("child_process").spawn; //used by run_module()

const load_description = (module) =>
      JSON.parse(fs.readFileSync(__dirname+"/"+[module, confFile].join('/')), 'utf-8');

const load_service = (module, serviceName) =>
      load_description(module).services[serviceName];

//const list_services = (module) =>
      
const get_directories = (path) =>
      fs.readdirSync(path).filter(
	  (src) => fs.lstatSync(src).isDirectory()
      );
/**
 * Lists all available modules
 **/
const list_all_modules = ()=>
      get_directories("./")
      .filter( dir => fs.existsSync([dir, confFile].join('/')) );	       

/**
 * Prepares commandline array for run_module()
 **/ 
const create_cmdline = (service, input) => {
    let parseInput = (e) =>
	{
	if(typeof e.val !== "undefined" && typeof input[e.val.substr(1)] !== "undefined")
 	    return "--"+e.key+' '+ input[e.val.substr(1)];
	return null;
    };

    var cmdline = [service.file, ...service.input.map(parseInput)];
    //var format = service.output.options[service.output.default];
    if (service.output.method == "file")
	cmdline = [...cmdline, "-o "+ service.output.location];
    return cmdline;
    //return cmdline.filter(n=>n); //n=>n to remove empty elements
};


const run_module = (module, serviceName, input) =>{
    var runpath = [__dirname,module].join('/');
    var service = load_service(module, serviceName);
    var cmdline = create_cmdline(service, input);
    var process = spawn(service.run, cmdline, {cwd: runpath});
    
    let consoleOut = (data) => {
	console.log(data.toString());
    };
    
    process.stdout.on('data',
		      consoleOut
		     );
    process.stderr.on('data',
		      consoleOut
		     );
    process.on('exit', (code) => {
	console.log(`Child process terminated with code ${code}`);
    });
    
};



module.exports = {
    load_service,
    list_all_modules,
    run_module
};
