import os
import cv2
import numpy

class DatasetReader:    
    def __init__(self, path="/home/itadmin/Public/datasets/lfw/"):
        self.type  = 'lfw'
        self.path = path

    def load_lfw_dataset(self):
        names = os.listdir(self.path)
        names.sort()
        i = 0
        self.imgpaths = []
        for name in names:
            p = [os.path.join(self.path, f) for f in os.listdir(self.path+name)]
            self.imgpaths.append(p) 
            i += 1
        self.names = names

    def getNames(self):
        return self.names

    def getImg(self):
        return self.imgpaths

    
    
