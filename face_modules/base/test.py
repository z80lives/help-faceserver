from  util import DatasetReader

ds = DatasetReader()

print("Loading LFW datesets..")
ds.load_lfw_dataset()
print("..Done")
