import cv2

font = cv2.FONT_HERSHEY_SIMPLEX

#def drawOutline(img, gray, faces):
#    for (x,y,w,h) in faces:
#        #i, confidence = recognizer.predict(gray[y:y+h, x:x+w])
#        confidence = 100 #100 - confidence
#        if confidence > 0:
#            cv2.rectangle(img,(x*2,y*2),(x*2+w*2,y*2+h*2),(255,0,0),2)
            #cv2.putText(img, str(names[i]), (x+5,y-5), font, 1, (255,255,255), 2)
            #cv2.putText(img, str(confidence), (x+5,y+h-5), font, 1, (255,255,0), 1)


def drawOutline(img, recOut):
    for el in recOut:
        x,y,w,h = el["face"]
        if el["confidence"] < 50:
            col = (0,0,255)            
        else:
            col = (255,0,0)
            cv2.putText(img, str((el["label"], el["confidence"])), (x+5,y-5), font, 1, (255,255,255), 2)
        cv2.rectangle(img,(x,y),(x+w,y+h), col,2)
    
class FaceModule:
    def __init__(self):
        self.recognizer = cv2.face.LBPHFaceRecognizer_create()
        self.face_cascade = cv2.CascadeClassifier('./cascades/haarcascade_frontalface_default.xml')

    def setImage(self, img, equalize=True):        
        self.img = img
        self.im_small = cv2.resize(img, (0,0), fx=0.5, fy=0.5)
        if equalize:
            self.histogramEqualize()
        self.im_small = cv2.cvtColor(self.im_small, cv2.COLOR_BGR2GRAY)

    def getImage(self):
        return self.im_small
    
    def histogramEqualize(self):
        img_yuv = cv2.cvtColor(self.im_small, cv2.COLOR_BGR2YUV)
        img_yuv[:,:,0] = cv2.equalizeHist( img_yuv[:,:,0] )
        self.im_small = cv2.cvtColor(img_yuv, cv2.COLOR_YUV2BGR)


    def loadModel(self, modelFile='trainer/model.yml'):
        self.recognizer.read(modelFile)

    def detectFaces(self):
        return self.face_cascade.detectMultiScale(self.im_small, 1.3, 5)

    def recognizeFace(self, rect):
        x,y,w,h = rect
        i, c = self.recognizer.predict(self.im_small[y:y+h, x:x+w])

    def rescaleRects(self, rects):
        return [el * 2 for el in rects]
    
    def recognizeFaces(self, faces):
        ret = []
        _str = "None"
        for(x,y,w,h) in faces:
            i,confidence = self.recognizer.predict(self.im_small[y:y+h, x:x+w])            
            ret.append({'face': (x*2,y*2,w*2,h*2), 'label': i, 'confidence': confidence})
        return ret
