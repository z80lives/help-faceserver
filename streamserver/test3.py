import numpy as np
import pickle
import cv2
from OpenFaceRecognizer import OpenFaceRecognizer
from frapi.dlibtools import DLIBAligner
from frapi.mtcnn import MTCNNDetector


import math
def sigmoid(x):
  return 1 / (1 + math.exp(-x))


of = OpenFaceRecognizer("/media/extra/models/classifiers/huit/classifier.pkl")
wc = cv2.VideoCapture(0)

detector = MTCNNDetector()
aligner = DLIBAligner()

#print("Loading embeddings")
#embeddings = of.loadEmbeddings("/tmp/aligned1/", False)
of.load_names("/tmp/aligned1/names.csv")
#embeddings = np.load("embeddings.npy")
with open('/tmp/embeddings.pkl', 'rb') as handle:
    embeddings = pickle.load(handle)

for x in range(0, 100):
    _, img = wc.read()

while True:
    _, img = wc.read()

    rect = detector.detect(img)

    if len(rect) > 0:
        r = rect[0]
        x1, y1, x2, y2 = r.left(), r.top(), r.right(), r.bottom()
        face = img[y1:y2, x1:x2]
        face = cv2.resize(face, (98, 98))

        aligned_face = aligner.align(face)

        if aligned_face is None:
            print("Cannot align")
        else:
            cv2.imshow("aligned", aligned_face)
            print(aligned_face.shape)
            label, conf = of.predict(aligned_face)
            print("Classifier result "+ label.decode() + " confidence = "+str(conf))

            rep = of.getEmbedding(aligned_face)

            possible_persons =  []
            for p in embeddings:
                sample_i = embeddings[p][0]
                test = rep
                d = test - sample_i
                dist = np.dot(d,d)
                dist2 = np.linalg.norm(d)
                if dist < 1:
                    possible_persons.append(p)
                print(p+",Squared Distance = {:0.3f}, euclidean dist={:0.3f}".format(dist, dist2))
            print(" ")
            print("Possible persons")
            print(possible_persons)
            if len(possible_persons) == 0:
                continue

            most_likely_persons = []
            print("Calculating the most likely people")
            score_board = {}
            for p in possible_persons:
                score_board[p] = 0
                for sample_i in embeddings[p]:
                    test = rep
                    d = test - sample_i
                    dist = np.dot(d,d)
                    dist2 = np.linalg.norm(d)
                    if dist >= 0.5:
                        score_board[p] += 1
                    if dist >= 1:
                        score_board[p] += 1 + sigmoid(dist)

                    print(p+",Squared Distance = {:0.3f}, euclidean dist={:0.3f}".format(dist, dist2))

            min_k = min(score_board, key=score_board.get)
            print("score board")
            print(score_board)

            print("Lowest score")
            print(score_board[min_k])
            identified = min_k

            print("Most likely person is "+identified)
            label = of.personToName(identified)
            print(" with the label "+ label)
            cv2.putText(aligned_face, label, (10, 10), cv2.FONT_HERSHEY_PLAIN, 1.0, (128,76, 250), 2)
            cv2.imshow("aligned", aligned_face)


            #cv2.imshow('face', aligned_face)
            #print(rec)
            cv2.waitKey()
    else:
        print("No face detected")

    #face = rects[]
    #aligner.align(face)
    #cv2.imshow('out', img)
    #cv2.imshow('face', face)
    if cv2.waitKey(1) == ord('q'):
        break


cv2.destroyAllWindows()
