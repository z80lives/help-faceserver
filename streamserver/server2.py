from PIL import Image
import cv2

from Recognizer import FaceModule, drawOutline, drawBoundingBox, drawSingleBoundingBox

import base64
import io
import tornado.web
import tornado.websocket
from tornado.ioloop import PeriodicCallback

print ("Loading face module")
faceModule = FaceModule()
print ("loading the model")
faceModule.loadModel()  #loads default model -- /trainer/model.yml 


class WebSocket(tornado.websocket.WebSocketHandler):
    def on_message(self, message):
        """Evaluates the function pointed to by json-rpc."""

        # Start an infinite loop when this is called
        if message == "read_camera":
            self.camera_loop = PeriodicCallback(self.loop, 10)
            self.camera_loop.start()

            
        # Extensibility for other methods
        else:
            print("Unsupported function: " + message)

    def loop(self):
        """Sends camera images in an infinite loop."""
        sio = io.BytesIO()

        _, frame = camera.read()            

        faceModule.setImage(frame)
        faces = faceModule.detectFaces()
        #recFaces = faceModule.recognizeFaces(faces)
        #drawOutline(frame, recFaces)

        drawBoundingBox(frame, faces, k=2)
        if len(faces) > 0:
            rout= faceModule.recognizeFace(faces[0])
            drawSingleBoundingBox(frame, rout['face'], rout['label'], k=2)
        
        #frame = faceModule.getImage()
        #img = Image.fromarray(cv2.cvtColor(frame, cv2.COLOR_GRAY2RGB))
        #for face in faces:
         #   x,y,w,h = face            
         #   break
        
        img = Image.fromarray(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
        #img = Image.fromarray(cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY))

        img.save(sio, "JPEG")

        try:
            self.write_message(base64.b64encode(sio.getvalue()))
        except tornado.websocket.WebSocketClosedError:
            self.camera_loop.stop()
    def check_origin(self, origin):
            return True


port = 8000
dev_id = 0

camera = cv2.VideoCapture(dev_id)
#camera.set
handlers = [(r"/websocket", WebSocket)]
application = tornado.web.Application(handlers)
application.listen(port)

print("Ready")
tornado.ioloop.IOLoop.instance().start()
