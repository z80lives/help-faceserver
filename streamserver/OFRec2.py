import cv2
import openface
openfacedir = "/media/extra/build/openface"
datasetdir = "/media/extra/datasets"

#https://stackoverflow.com/questions/6893968/how-to-get-the-return-value-from-a-thread-in-python

#from concurrent.futures import ProcessPoolExecutor
#import time
from frapi.dlibtools import DLIBDetector, DLIBAligner
#from frapi.dlibtools import CorrelationTracker as DLIBTracker

from frapi.mtcnn import MTCNNDetector

import dlib
from common.sort import Sort
from OpenFaceRecognizer import OpenFaceRecognizer as OFP
import uuid

model_path="/home/itadmin/Public"
class FrameProcessor():
#    def __init__(self, model=model_path+"/models/classifiers/huit/classifier.pkl"):
    def __init__(self, model="/tmp/featureDir/classifier.pkl"):
        self.detector = MTCNNDetector()
        self.detector2 = DLIBDetector()
        self.aligner = DLIBAligner()
        self.rtracker = Sort()
#        self.tracker = DLIBTracker()
        #self.rec = OFP("../facerec2/tdir/classifier.pkl")
        try:
            self.rec = OFP(model)
        except:
            print("Cannot find the model. Check whether "+model+" exists")
            exit()
        self.async_tasks = []
        self.async_results = []
        self.prepareAsyncTasks()
        self.tracker_data = {}

    def drawBorder(self, img, r):
        cv2.rectangle(img, (r.left(), r.top()), (r.right(), r.bottom()), (255,0,0), 2)

    def drawText(self, img, pos, text):
        cv2.putText(img, text, (pos[0]+5,pos[1]+5), cv2.FONT_HERSHEY_PLAIN, 1.0, (128,76, 250), 2)

    def prepareAsyncTasks(self):
        self.async_tasks.append(None) #detector tasks
        self.async_results.append([]) #detector tasks
        self.async_tasks.append(None) #align tasks
        self.async_results.append([]) #align  tasks
        self.async_tasks.append(None) #predict tasks
        self.async_results.append([]) #predict  tasks


    def startAsyncTasks(self, in_img):
#        gray_img = cv2.cvtColor(in_img, cv2.COLOR_BGR2GRAY)
        rgb_img = cv2.cvtColor(in_img, cv2.COLOR_BGR2RGB)

        if self.async_tasks[0] is None:
            self.async_tasks[0] = self.detector.detect_async(rgb_img)
        if self.async_tasks[1] is None:
            self.async_tasks[1] = self.aligner.align_async(rgb_img, self.async_results[0])
        if self.async_tasks[2] is None:
            self.async_tasks[2] = self.rec.predict_multi_async(self.async_results[1])

    def runAsyncTasks(self):
        for i, task in enumerate(self.async_tasks):
            if task is None:
                continue
            if task.done():
                self.async_results[i] = task.result()
                self.async_tasks[i] = None

    def process_image(self, in_img):
        out_img = in_img.copy()
        self.startAsyncTasks(in_img)
        self.runAsyncTasks()

        #print("Average detection={}".format(self.detector.detection_time))

        trackers = self.rtracker.update(self.detector.sort_result)


        print(self.async_results[2])
        newData = {i:x for i,x in enumerate(self.async_results[2])}

        for i in newData:
            if i not in self.tracker_data:
                self.tracker_data[i] = newData[i]
            #elif newData[i][0] == self.tracker_data[i][0]:
            #    if newData[i][1] >= self.tracker_data[i][1]:
            #        self.tracker_data[i] = newData[i]
            else:
                self.tracker_data[i] = newData[i]
                #if(newData[i][

        #print(trackers)
        #rects = self.async_results[0]
        #for i,rect in enumerate(rects):
        #    self.drawBorder(out_img, rect)
#        for rimg in self.async_results[1]:
#            print(rimg)
#           cv2.imshow('aligned', rimg)


#        self.alignQueue = []

        #if len(self.rtracker.trackers) >= 1:
        #    print(self.rtracker.trackers[0])
        for i, d in enumerate(trackers):
            box_id  = d[4]
            x1,y1,x2,y2 = int(d[0]), int(d[1]), int(d[2]), int(d[3])

            self.drawBorder(out_img, dlib.rectangle(x1,y1,x2,y2))

            if x2-x1 <=96 and y2-y1 <=96:
                self.drawText(out_img, (x1,y2), "Too far"  )

            if i in self.tracker_data:
                if self.tracker_data[i] is None:
                    continue
                person_name = self.tracker_data[i][0].decode()
                #person_name = self.rec.personToName(person_name)
                self.drawText(out_img, (x2,y1), person_name + ","+ str(int(self.tracker_data[i][1]*100))  )

            #cImg = in_img[y1:y2, x1:x2]
            #cImg = self.aligner.align(cImg)
            #if not( cImg is None):
                #res, conf = self.rec.predict(cImg)
                #if conf >= 0.55:
            self.drawText(out_img, (x1,y1), "#{}".format(box_id)  )


        return out_img

