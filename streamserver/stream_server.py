#!/usr/bin/python3
import base64
import cv2
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('dev', type=str,
                   help='Media device to take input from')

#parser.add_argument('module', type=str,
#                   help='Name of the face module')

#parsr.add
args = parser.parse_args()
#modname = args.module

videosrc = args.dev
vidstream = cv2.VideoCapture(videosrc);
ret, _ = vidstream.read()

if not ret:
    print("Cannot read from input stream device")
    exit()

#import numpy as np
#import websocket
import PIL.Image as Image
import atexit
import io
import tornado.web
import tornado.websocket
#from tornado.websocket import websocket_connect
from tornado.ioloop import PeriodicCallback
#from ForwardStream2 import WebSocket

#import threading

#import common.inception_resnet_v1

from OFRec_basic import FrameProcessor
#from OFRec import FrameProcessor
#from face_modules.loader import ModLoader

import signal
import sys
def signal_handler(sig, frame):
        print('You pressed Ctrl+C!')
        sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)
#signal.pause()

fp = FrameProcessor()

class WebSocket(tornado.websocket.WebSocketHandler):
    def on_open(self):
        print("Starting out socket")

    def on_close(self):
        print("Closing websocket")
        
    def on_message(self, message):
        if message == "read_camera":
            print("Reading from in stream")
            self.camera_loop = PeriodicCallback(self.loop, 10)
            self.camera_loop.start()
        else:
            print("Unsupported function: "+message)

    def setSharedVar(self, sv):
        self.shared_vars = sv[0]

    def loop(self):
        sio = io.BytesIO()
        
        read, img = vidstream.read()
        if not read:
            print("Cannot read from stream device: "+ videosrc)
            return

        img = fp.process_image(img)

        cv2_im = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

        img = Image.fromarray(cv2_im)
        img.save(sio, "JPEG")
        try:
            self.write_message(base64.b64encode(sio.getvalue()))
        except tornado.websocket.WebSocketClosedError:
            print("Error occured")
            self.camera_loop.stop()
            self.finish()

    def check_origin(self, origin):
        return True


def quit_gracefully():
    print("Closing stream service...")
    #ws.close()

atexit.register(quit_gracefully)

class OutWSServer(WebSocket):
    def handleMessage(self):
        # echo message back to client
        print(self.data)
        self.sendMessage("AOK")
        #self.sendMessage(self.data)

    def handleConnected(self):
        print(self.address, 'connected')

    def handleClose(self):
        print(self.address, 'closed')

if __name__ == "__main__":
#    modloader = ModLoader(modname)
    port = 4001
    handlers = [(r"/websocket", WebSocket)]
    application = tornado.web.Application(handlers)
    application.listen(port)
    print("Starting out socket")
    tornado.ioloop.IOLoop.instance().start()
