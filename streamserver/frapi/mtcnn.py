from mtcnn.mtcnn import MTCNN
import cv2
from frapi.multithread import threadpool

from common.utils.caltools import incAverage
import time
import dlib

import numpy as np

class MTCNNDetector:
    def __init__(self):
        self.detector = MTCNN()
        self.detection_time = 0
        self.detection_count = 0
        self.padding = 13
        #self._tw = 474
        #self._th = 224
        self._tw = 160
        self._th = 120
        self.results = []
        self.sort_result = []

#        self.confidence = 0.1

    def dlibRectsToSort(self, rects):
        res = rects
        return np.array([[res[0][i].left(), res[0][i].top(), res[0][i].right(), res[0][i].bottom(), res[1][i]]  for i in range(0, len(res[0]))])


    def detect(self, frame):
        #inframe = cv2.resize(frame, (474, 224))
        #inframe=frame
        inframe = cv2.resize(frame, (self._tw, self._th))
        #start_time = time.time
        start_time = time.time()
        detect_results = self.detector.detect_faces(inframe)
        time_taken = time.time() - start_time

        if self.detection_count < 100:
            self.detection_count += 1
            self.detection_time = incAverage(self.detection_time, time_taken, self.detection_count)



        results = []
        sw,sh = self.getScaleRatio(frame)
        padding = self.padding
        self.sort_result=[]
        for res in detect_results:
            x,y,w,h = res['box']
            x1, y1, x2, y2 = ( int(x*sw)-padding, int(y*sh)-padding, int((x+w)*sw)+padding, int((y+h)*sh) +padding )
#            if res['confidence'] > self.confidence:
            results.append(dlib.rectangle(x1,y1,x2,y2))
            self.sort_result.append([x1, y1, x2,y2, res['confidence']])

        self.sort_result = np.array(self.sort_result)
        #self.sort_results = [ e['box']+ [detect_results['confidence']] for i in range(0,len(detect_results)]

        return dlib.rectangles(results) #detect_results

    def getScaleRatio(self, frame):
        _w = frame.shape[1]
        _h = frame.shape[0]
        return _w/self._tw, _h/self._th

    '''
    Rescales (x,y,w,h) to the original image
    '''
    def rescale(self, frame, box):
        xs,ys = self.getScaleRatio(frame)
        x,y,w,h = box
        return int(x*xs), int(y*ys), int(w*xs), int(h * ys)

    @threadpool
    def detect_async(self, frame):
        return self.detect(frame)
