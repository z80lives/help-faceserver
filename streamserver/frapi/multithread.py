from functools import wraps

from concurrent.futures import ThreadPoolExecutor
_DEFAULT_POOL = ThreadPoolExecutor(max_workers=3)
def threadpool(f, executor=None):
    @wraps(f)
    def wrap(*args, **kwargs):
        return (executor or _DEFAULT_POOL).submit(f, *args, **kwargs)
    return wrap
