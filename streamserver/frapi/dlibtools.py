from frapi.multithread import threadpool

from common.utils.caltools import incAverage
import dlib
import time
import numpy as np
import cv2

#openface_path = "/home/itadmin/build/openface"
#openface_path = "/media/extra/build/openface"
openface_path = "./common"
class DLIBDetector:
    def __init__(self):
        self.detector = dlib.get_frontal_face_detector()
        self.detection_time = 0
        self.detection_count = 0
        self.result_confidence = []
        self.results = []
        self.threshold = 0.4
        self.sort_result = []

    def detect(self, img):
        start_time = time.time()
        result = self.detector.run(img, 1, self.threshold)
        time_taken = time.time() - start_time

        if self.detection_count < 100:
            self.detection_count += 1
            self.detection_time = incAverage(self.detection_time, time_taken, self.detection_count)

        self.result_confidence = result[1]
        self.results = result
        self.sort_result = self.dlibRectsToSort(result)

        return result[0]

    @threadpool
    def detect_async(self, img):
        return self.detect(img)

    def dlibRectsToSort(self, rects):
        res = rects
        return np.array([[res[0][i].left(), res[0][i].top(), res[0][i].right(), res[0][i].bottom(), res[1][i]]  for i in range(0, len(res[0]))])

import openface #use openface aligner
class DLIBAligner:
    def __init__(self):
        self.aligner = openface.AlignDlib(openface_path+"/models/dlib/shape_predictor_68_face_landmarks.dat")

    def align(self, img):
        return self.aligner.align(96, img, landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)


    def align_multi(self, img, bb):
        alignedFaces = []
        for rect in bb:
            x1,y1,x2,y2 = rect.left(), rect.top(), rect.right(), rect.bottom()

            alignedFace = None

            if True:#x2-x1 >= 96 and y2 - y1 >= 96:
                cutImg = img[y1:y2, x1:x2]
                #cutImg = cv2.resize(cutImg, (96,96))

                alignedFace = self.aligner.align(96, cutImg, landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)

            alignedFaces.append(alignedFace)
        return alignedFaces

    @threadpool
    def align_single_async(self, img):
        return self.align(img)

    @threadpool
    def align_async(self, img, bb):
        return self.align_multi(img, bb)

class DLIBTracker:
    def __init__(self, rect, img):
        self.tracker = dlib.correlation_tracker()
        self.tracker.start_track(img, rect)
        self.confidence = 0

        self.time_since_update = 0



#retrieved from: https://github.com/ZidanMusk/experimenting-with-sort/blob/master/correlation_tracker.py
"""
        @author: Mahmoud I.Zidan
"""

from dlib import correlation_tracker, rectangle

'''Appearance Model'''
class CorrelationTracker:

  count = 0
  def __init__(self,bbox,img):
    self.tracker = correlation_tracker()
    self.tracker.start_track(img,rectangle(long(bbox[0]),long(bbox[1]),long(bbox[2]),long(bbox[3])))
    self.confidence = 0. # measures how confident the tracker is! (a.k.a. correlation score)

    self.time_since_update = 0
    self.id = CorrelationTracker.count
    CorrelationTracker.count += 1
    self.hits = 0
    self.hit_streak = 0
    self.age = 0

  def predict(self,img):
    self.confidence = self.tracker.update(img)

    self.age += 1
    if (self.time_since_update > 0):
      self.hit_streak = 0
    self.time_since_update += 1

    return self.get_state()

  def update(self,bbox,img):
    self.time_since_update = 0
    self.hits += 1
    self.hit_streak += 1

    '''re-start the tracker with detected positions (it detector was active)'''
    if bbox != []:
      self.tracker.start_track(img, rectangle(long(bbox[0]), long(bbox[1]), long(bbox[2]), long(bbox[3])))
    '''
    Note: another approach is to re-start the tracker only when the correlation score fall below some threshold
    i.e.: if bbox !=[] and self.confidence < 10.
    but this will reduce the algo. ability to track objects through longer periods of occlusions.
    '''

  def get_state(self):
    pos = self.tracker.get_position()
    return [pos.left(), pos.top(),pos.right(),pos.bottom()]


