import base64
import cv2

import numpy as np
import websocket
import PIL.Image as Image
import atexit
import io
import tornado.web
import tornado.websocket
from tornado.websocket import websocket_connect
from tornado.ioloop import IOLoop, PeriodicCallback
from ForwardStream2 import WebSocket
import threading

from SimpleWebSocketServer import SimpleWebSocketServer, WebSocket


import signal
import sys
def signal_handler(sig, frame):
        print('You pressed Ctrl+C!')
        sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)
#signal.pause()


try:
    import thread
except ImportError:
    import _thread as thread
import time



    

from io import StringIO, BytesIO

img = None


def quit_gracefully():
    print("Closing websocket...")
    #ws.close()
    
atexit.register(quit_gracefully)

def on_message(ws, m):
    img =  readb64(m)
    print(len(img))
    print("Message received")

def on_error(ws, error):
    print(error)


def on_close(ws):
    print("### closed ###")

def on_open(ws):    
    def run(*args):
        ws.send("read_camera")
    thread.start_new_thread(run, ())


class OutWSServer(WebSocket):
    def handleMessage(self):
        # echo message back to client
        print(self.data)
        self.sendMessage("AOK")
        #self.sendMessage(self.data)

    def handleConnected(self):        
        print(self.address, 'connected')

    def handleClose(self):
        print(self.address, 'closed')

    
#https://stackoverflow.com/questions/33754935/read-a-base-64-encoded-image-from-memory-using-opencv-python-library
def readb64(base64_string):
    sbuf = BytesIO()
    sbuf.write(base64.b64decode(base64_string))
    pimg = Image.open(sbuf)
    return cv2.cvtColor(np.array(pimg), cv2.COLOR_RGB2BGR)


if __name__ == "__main__":
    port = 4001       
    handlers = [(r"/websocket", WebSocket)]
    application = tornado.web.Application(handlers)
    application.listen(port)
    print("Starting out socket")
    tornado.ioloop.IOLoop.instance().start()
    
    
    #t1.daemon = True    
    #t1.start()
    
    #t2.setDaemon(True)
    #t2.start()

    #dev_id = 

    #print("Starting in socket")
    #ws.run_forever()

    #ws.


