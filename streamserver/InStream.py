import base64
import cv2
import numpy as np
import io
import tornado.web
import tornado.websocket
from tornado.ioloop import PeriodicCallback

class WebSocket(tornado.websocket.WebSocketHandler):
    def __init__(self):
        self.img = np.zeros((height,width,3), np.uint8)

    def on_message(self, message):
        if message == "read_camera":
            self.camera_loop = PeriodicCallback(self.loop, 10)
            self.camera_loop.start()
        else:
            print("Unsupported function: "+message)


    def loop(self):
        sio = io.BytesIO()
        img = self.img
        img.save(sio, "JPEG")
        try:
            self.write_message(base64.b64encode(sio.getvalue()))
        except tornado.websocket.WebSocketClosedError:
            self.camera_loop.stop()

    def check_origin(self, origin):
        return True

