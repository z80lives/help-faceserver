import cv2
import argparse
import loader
import os


path = os.path.dirname(os.path.realpath(__file__))
ml = loader.ModLoader(mod_dir=path)

parser = argparse.ArgumentParser()
parser.add_argument(
        '--input', type=str,
        help="Input image")
parser.add_argument('--output', type=str, help="Output Image")
parser.add_argument('--module', type=str, help="Module name")
parser.add_argument('--name', type=str, help='Frame Processor ID')

display_out = False
args=parser.parse_args()

if args.output is None:
    display_out = True

print("Reading "+args.input)
in_img = cv2.imread(args.input)

fpclass = ml.load_frame_processor(args.module, args.name)
fp = fpclass()
out_img = fp.process_image(in_img)

if not display_out:
    print("Writing "+args.output)
    cv2.imwrite(args.output, out_img)
else:
    cv2.imshow('display', out_img)
    cv2.waitKey(0)
