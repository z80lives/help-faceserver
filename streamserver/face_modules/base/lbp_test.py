import cv2
import numpy as np

cap = cv2.VideoCapture(0)

def drawOutline(img, faces):
    for (x,y,w,h) in faces:
        cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)


#face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
#eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')
face_cascade = cv2.CascadeClassifier('../try2/cascades/haarcascade_frontalface_default.xml')

while(True):    
    ret, img = cap.read()
    img = cv2.flip(img ,1)
    
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    #lbh = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    #vis = np.zeros((480, 640), np.float32)
    #cv.CvtColor(vis0, vis2, cv.CV_GRAY2BGR)
    #h,w = vis.shape
    
    cv2.imshow('wcframe', gray)
    
    if cv2.waitKey(1) & 0xFF == ord('c'):        
        vis = cv2.cvtColor(img, cv2.CV_8UC3) #scaled
        vis = cv2.cvtColor(vis, cv2.CV_32FC3)  #non-scaled

        
        #inp = cv2.cvtColor(gray, cv2.CV_8UC1)
        h,w = vis.shape
        #inp = vis.reshape((h,w,1))
        inp = vis.resize(320,200)
        for y in range(1, 319):
            for x in range(1, 199):
                c = inp[y,x]
                
                #p = 255 - p
                p = 0
                p |= (inp[y-1,x-1] > c)  << 0
                p |= (inp[y-1,x] > c) << 1
                p |= (inp[y-1,x+1] > c) << 2

                p |= (inp[y,x-1] > c)  << 3
                p |= (inp[y,x+1] > c) << 4
            
                p |= (inp[y+1,x-1] > c)  << 5
                p |= (inp[y+1,x] > c) << 6
                p |= (inp[y+1,x+1] > c) << 7
                #p *= 255
                vis[y,x] = p
                
        
        cv2.imshow('out', vis)
        #vis[y,x] = gray[y,x]

    #hsv[:,:,2] += 100
    #test = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
    #gray = cv2.cvtColor(test, cv2.COLOR_BGR2GRAY)
    
    #faces = face_cascade.detectMultiScale(gray, 1.3, 5)


    #cv2.imshow('wcTest', test)
    #drawOutline(img, faces)

    #close on q
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break


    
cap.release()
cv2.destroyAllWindows()
