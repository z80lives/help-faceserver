import cv2

font = cv2.FONT_HERSHEY_SIMPLEX

#def drawOutline(img, gray, faces):
#    for (x,y,w,h) in faces:
#        #i, confidence = recognizer.predict(gray[y:y+h, x:x+w])
#        confidence = 100 #100 - confidence
#        if confidence > 0:
#            cv2.rectangle(img,(x*2,y*2),(x*2+w*2,y*2+h*2),(255,0,0),2)
            #cv2.putText(img, str(names[i]), (x+5,y-5), font, 1, (255,255,255), 2)
            #cv2.putText(img, str(confidence), (x+5,y+h-5), font, 1, (255,255,0), 1)


def drawOutline(img, recOut):
    for el in recOut:
        x,y,w,h = el["face"]
        if el["confidence"] < 50:
            col = (0,0,255)            
        else:
            col = (255,0,0)
            cv2.putText(img, str((el["label"], el["confidence"])), (x+5,y-5), font, 1, (255,255,255), 2)
        cv2.rectangle(img,(x,y),(x+w,y+h), col,2)

class FaceModule:
    def __init__(self):
        self.recognizer = cv2.face.LBPHFaceRecognizer_create()
        self.face_cascade = cv2.CascadeClassifier('./cascades/haarcascade_frontalface_default.xml')

    def setImage(self, img, equalize=True):        
        self.img = img
        self.im_small = cv2.resize(img, (0,0), fx=0.5, fy=0.5)
        if equalize:
            self.histogramEqualize()
        self.im_small = cv2.cvtColor(self.im_small, cv2.COLOR_BGR2GRAY)

    def getImage(self):
        return self.im_small
    
    def histogramEqualize(self):
        img_yuv = cv2.cvtColor(self.im_small, cv2.COLOR_BGR2YUV)
        img_yuv[:,:,0] = cv2.equalizeHist( img_yuv[:,:,0] )
        self.im_small = cv2.cvtColor(img_yuv, cv2.COLOR_YUV2BGR)


    def loadModel(self, modelFile='trainer/model.yml'):
        self.recognizer.read(modelFile)

    def detectFaces(self):
        return self.face_cascade.detectMultiScale(self.im_small, 1.3, 5)

    def recognizeFace(self, rect):
        x,y,w,h = rect
        i, c = self.recognizer.predict(self.im_small[y:y+h, x:x+w])
        return i,c

    def rescaleRects(self, rects):
        return [el * 2 for el in rects]
    
    def recognizeFaces(self, faces):
        ret = []
        _str = "None"
        for(x,y,w,h) in faces:
            i,confidence = self.recognizer.predict(self.im_small[y:y+h, x:x+w])            
            ret.append({'face': (x*2,y*2,w*2,h*2), 'label': i, 'confidence': confidence})
        return ret

    
import os
import errno
class FModule:
    def __init__(self, config):
        self.config = config

    def createLabelIndex(self, labels):
        '''
         Converts string label array to int array.

        For backward compatibility with OpenCV's classifier.

        For example
        ------------
        ["Ben", "Ben", "Kevin", "Rahimi", "Rahimi"] will return [0, 0, 1, 2, 2].
        '''
        ret = []
        lastName = None
        counter = 0
        for name in labels:
            if lastName is None:
                lastName = name
            if name != lastName:
                counter += 1
                lastName =  name
            ret.append(counter)
        return ret
        
        
    def loadDataset(self, name, label="folder", labels="dir", loadImages=False, grayscale=False):
        dset = self.config['dataset']
        path = dset['path']+name
        #fPaths = [os.path.join(path,f) for f in os.listdir(path)]
        labels = []
        d = []
        if label == "folder":
            classes = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]
            for c in classes:
                filepath = os.listdir( os.path.join(path, c))
                for f in filepath:
                    labels.append(c)
                    cf = c+"/"+f
                    d.append(os.path.join(path, cf) )
        else:
            return None
        if loadImages:
            imgs = []
            for f in d:
                if grayscale:
                    img = cv2.imread(f,0)
                else:
                    img = cv2.imread(f)
                imgs.append(img)
            d = imgs
        return [labels, d]

    def createPath(self, filename):
        if not os.path.exists(os.path.dirname(filename)):
            try:
                os.makedirs(os.path.dirname(filename))
            except OSError as exc: # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise
    
import numpy as np
import csv
import json

class BaseRecognizerAPI(FModule, FaceModule):
    def __init__(self, config):
        FModule.__init__(self,config)
        FaceModule.__init__(self)
        #super(BaseRecognizerAPI, self).__init__(config)
        self.datasetdir="/tmp/"
        self.modeldir = "/tmp/"
        self.labelFile = "/labels.csv"
        self.modelFile = "/model.yml"
        self.metaFile = "/meta.json"

        
    def train(self, args):
        datasetdir = self.datasetdir+args['output']
        modeldir = self.datasetdir+args['output']
        labelFile = datasetdir + self.labelFile 
        modelFile = modeldir + self.modelFile
        metaFile = modeldir + self.metaFile 

        dataset =  self.loadDataset(args['name'], args["label"], loadImages=True, grayscale=True)
        labelIndex = self.createLabelIndex(dataset[0])

        #create meta file
        ##meta file contains information about the model
        if 'meta' in args:
            meta = args['meta']
            meta['classifier_name'] = "OpenCV/K-Nearest"
            meta['labelfile'] = labelFile
            with open(metaFile, 'w') as f:
                json.dump(meta, f)
            
        
        #create the label file
        labelMap = dict(zip(labelIndex, dataset[0]))
        self.createPath(labelFile)
        with open(labelFile, "w") as f:
            w = csv.writer(f)
            w.writerows(labelMap.items())
            
        #train the 
        self.recognizer.train(dataset[1], np.array(labelIndex))                
        self.recognizer.save(modelFile)
        return self.recognizer

    def predict(self, args):
        modeldir = self.datasetdir+args['model']
        labelFile = modeldir + self.labelFile 
        modelFile = modeldir + self.modelFile
        metaFile = modeldir + self.metaFile 

        #read labelFile
        ##meta file contains information about the model
        try:
            meta = None
            with open(metaFile, 'r') as f:
                meta = json.load(f)
        except:
            pass

        labelMap = None
        if meta is not None and 'labelfile' in meta:            
            with open(labelFile, mode='r') as infile:
                reader = csv.reader(infile)
                labelMap = {rows[0]:rows[1] for rows in reader}
        
        if "image" in args and 'type' in args['image']:
            if args["image"]["type"] == "file":                           
                self.loadModel(modelFile)
                img = cv2.imread(args['image']['data'])

                self.setImage(img)
                #print(labelFile)
                p, c =self.recognizer.predict(self.im_small)
                print(labelMap)
                if not(labelMap is None):
                    return labelMap[str(p)], c
                else:
                    return p, c
        else:
            raise Exception("Please declare a valid type.")
            

        return None

#testImg = 
#b = BaseRecognizerAPI({"dataset": {"path":"/media/extra/datasets/", "type": "lfw"}, "model_dir": "/media/extra/models/"})

    
