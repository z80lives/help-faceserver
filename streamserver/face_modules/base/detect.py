import sys
import cv2

#import numpy
from Recognizer import FaceModule
rec = FaceModule()

params = [p.split(' ') for p in sys.argv[1:] ]
outparams = {i[0][1:] : i[1] for i in params if i[0][:2] != '--'}
params = {i[0][2:] : i[1] for i in params if i[0][:2] == '--'}


print("loading file "+params['input'])
img = cv2.imread(params['input'])
rec.setImage(img)
faces = rec.rescaleRects(rec.detectFaces())
#faces=rec.detectFaces()
x,y,w,h = faces[0]
cv2.imwrite(outparams['o'], img[y:y+h, x:x+w])
print("saving file to "+outparams['o'])

