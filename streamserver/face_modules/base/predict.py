import argparse
#import cv2
import sys
print(sys.argv)
print()

params = [p.split(' ') for p in sys.argv[1:] ]
outparams = {i[0][1:] : i[1] for i in params if i[0][:2] != '--'}
params = {i[0][2:] : i[1] for i in params if i[0][:2] == '--'}

print("Classifying "+params['input'])
print("  Model file: "+params['classifier'])
print("Output "+outparams['o'])
