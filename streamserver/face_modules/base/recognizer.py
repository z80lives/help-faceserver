import cv2
#import numpy

#from Recognizer import drawOutline
from Recognizer import FaceModule, drawOutline

#cap = cv2.VideoCapture(0)
cap = cv2.VideoCapture("/media/extra/datasets/sample/court.mp4")
#cap = cv2.VideoCapture("http://192.168.0.127:8080/video")
#cap = cv2.VideoCapture("http://10.125.192.146:8080/video")

names = ["Unknown", "Ben", "Rahimi", "Ibrahim", "Kevin", "Talha", "Yazan"]
rec = FaceModule()

rec.loadModel()

while(True):
    ret, img = cap.read()
    img = cv2.flip(img ,1)


    rec.setImage(img)

    faces = rec.detectFaces()
    recFaces = rec.recognizeFaces(faces)
    drawOutline(img, recFaces)

    cv2.imshow('wqcframe', img)
    cv2.imshow('test', rec.getImage())
    
    #close on q
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
