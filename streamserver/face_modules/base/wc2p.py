import cv2
import numpy

cap = cv2.VideoCapture("http://192.168.0.127:8080/video")

def drawOutline(img, faces):
    for (x,y,w,h) in faces:
        cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)


#face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
#eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')
face_cascade = cv2.CascadeClassifier('../try2/cascades/haarcascade_frontalface_default.xml')

while(True):    
    ret, img = cap.read()
    img = cv2.flip(img ,1)
    
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    #hsv[:,:,2] += 100
    #test = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
    #gray = cv2.cvtColor(test, cv2.COLOR_BGR2GRAY)
    
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)


    #cv2.imshow('wcTest', test)
    drawOutline(img, faces)
    cv2.imshow('wcframe', img)
    #close on q
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    
cap.release()
cv2.destroyAllWindows()
