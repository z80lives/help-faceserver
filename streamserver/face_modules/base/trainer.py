import cv2
import numpy
import os


path = "dataset"
face_cascade = cv2.CascadeClassifier('../try2/cascades/haarcascade_frontalface_default.xml')

recognizer = cv2.face.LBPHFaceRecognizer_create()



def getImagesAndLabels(path):
    imgPaths = [os.path.join(path,f) for f in os.listdir(path)]
    ids = []
    samples = []    
    for imgPath in imgPaths:
        img =  cv2.imread(imgPath,0)
        id = int(os.path.split(imgPath)[-1].split(".")[1])
        faces = face_cascade.detectMultiScale(img)
        for(x,y,w,h) in faces:
            samples.append(img[y:y+h, x:x+w])
            ids.append(id)
    return samples, ids

samples, ids = getImagesAndLabels(path)
recognizer.train(samples, numpy.array(ids))
recognizer.save('trainer/model.yml')
