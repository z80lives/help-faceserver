var confFile = "module.json"; //default configuration file to search for
const config =  require('../../config/server.json');

var fs = require('fs'); //used by load_description(), get_directories()
var spawn = require("child_process").spawn; //used by run_module()
const util = require('util');
var python_shell = require('python-shell');

const load_description = (module) =>
      JSON.parse(fs.readFileSync(__dirname+"/"+[module, confFile].join('/')), 'utf-8');

const load_service = (module, serviceName) =>
      load_description(module).services[serviceName];

const service_info = (m, s) =>
      load_description(m).services[s];

const list_services = m => Object.keys(load_description(m).services);

//const list_services = (module) =>
      
const get_directories = (path) =>
    fs.readdirSync(path)
    .filter(
	  (src) => fs.lstatSync(path+src).isDirectory()
      );

/**
 * Lists all available modules
 **/
const list_all_modules = ()=>
      get_directories(__dirname+"/")
      .filter( dir => fs.existsSync([__dirname, dir, confFile].join('/')) );	       
/**
 * Prepares commandline array for run_module()
 **/ 
const create_cmdline = (service, input) => {
    let parseInput = (e) =>
	{
	if(typeof e.val !== "undefined" && typeof input[e.val.substr(1)] !== "undefined")
 	    return "--"+e.key+' '+ input[e.val.substr(1)];
	return null;
    };

    var cmdline = [service.file, ...service.input.map(parseInput)];
    //var format = service.output.options[service.output.default];
    if (service.output.method == "file")
	cmdline = [...cmdline, "-o "+ service.output.location];
    return cmdline;
    //return cmdline.filter(n=>n); //n=>n to remove empty elements
};


const run_module = (module, serviceName, input) =>{
    var runpath = [__dirname,module].join('/');
    var service = load_service(module, serviceName);
    var cmdline = create_cmdline(service, input);
    var process = spawn(service.run, cmdline, {cwd: runpath});
    
    let consoleOut = (data) => {
	console.log(data.toString());
    };
    
    process.stdout.on('data',
		      consoleOut
		     );
    process.stderr.on('data',
		      consoleOut
		     );
    process.on('exit', (code) => {
	console.log(`Child process terminated with code ${code}`);
    });
    
};


//const service_ = (m, s) =>
//      load_description(m).services[s];
const get_frame_processor_info = (module, name) => {
    let desc = load_description(module);
    let found = null;
    desc.frame_processors.forEach(f => {if(f['id'] == name) found = f;});
    return found;
};

const run_frame_processor = (workDir, module, name, success) => {    
    var options = {
	pythonPath: config.python.path,
	mode: 'text',
	scriptPath: __dirname,	
	args: ["--input", util.format("%s/input.jpg", workDir),
	       "--output", util.format("%s/output.jpg", workDir),
	       "--module", util.format("%s", module),
	       "--name", util.format("%s", name),
	      ]
    };
    
    var ps = new python_shell('test_frame_processor.py', options);
    //var out = "";
    
    ps.on('message', (msg) => {
	console.log(msg);
    });
    
    ps.end((err, code, signal) => {
	if(err)
	    success(err);
	success("SUCCESS!");
    });

};

module.exports = {
    load_description,
    load_service,
    list_all_modules,
    list_services,
    service_info,
    get_frame_processor_info,
    run_frame_processor,
    run_module
};
