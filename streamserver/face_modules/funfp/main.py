import cv2

class RevColor:
    def process_image(self, in_img):
        out_img = cv2.cvtColor(in_img, cv2.COLOR_BGR2GRAY)
        return out_img

class CartoonFP:
    def process_image(self, in_img):
        #img = cv2.cv2Color(in_img, cv2.COLOR_BGR2R)
        img=in_img
        img_color = cv2.bilateralFilter(img, d=9,sigmaColor=9,sigmaSpace=7)

        img_rgb = cv2.cvtColor(img_color, cv2.COLOR_BGR2RGB)
        img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2GRAY)
        img_blur = cv2.medianBlur(img_gray, 7)

        img_edge = cv2.adaptiveThreshold(img_blur, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, blockSize=9, C=2)

        img_edge = cv2.cvtColor(img_edge, cv2.COLOR_GRAY2BGR)
        img_cartoon = cv2.bitwise_and(img_color, img_edge)

        return img_cartoon
