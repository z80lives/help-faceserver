import os
import json
import glob
import importlib

from itertools import chain

_debug_mode = True

default_config = {"dataset": {"path":"/media/extra/datasets/", "type": "lfw"}, "model_dir": "/media/extra/models/"}

'''
 Provides identical functionalities as the javascript file, mod_loader.js
'''
class ModLoader:
    def __init__(self, mod_dir = "./face_modules"):
        self.confFile = "module.json"
        self.path = mod_dir


    def list_all_modules(self):
        """
        Lists all modules available.
        """

        return [p[len(self.path)+1:-12] for p in glob.glob(self.path+"/*/module.json")]


    def load_description(self, module):
        '''
        Loads the description file and returns the configuration as a variable.

        Returns None on failure
        '''
        obj = None
        cfile = self.path+"/"+module+"/module.json"
        try:
            with open(cfile, "r") as f:
                buff = f.read()
                obj = json.loads(buff)
                return obj
        except IOError as error:
            if(_debug_mode):
                print("Module "+module+" not found")
                print(error)

    def load_service_description(self, module, service):
        '''
        Loads the description of the service
        '''
        info = self.load_description(module)
        if info['services'] and service in info['services']:
            return info["services"][service]
        else:
            raise Exception("Service '{}' not found in module '{}'".format(service, module))
        return None


    def get_services(self, module):
        '''
        Returns an array of all services provided by the module.

        Raises an exception if description file cannot be loaded.
        '''
        desc = self.load_description(module)
        if desc is None:
            raise Exception("Cannot load description file for"+module)

        if not 'services' in desc:
            return []
        return [k for k in desc['services']]


    def get_frame_processors(self, module):
        '''
        Returns an array of all frame processors provided by the module.

        Raises an exception if description file cannot be loaded.
        '''
        desc = self.load_description(module)
        if desc is None:
            raise Exception("Cannot load description file for "+module)

        if not 'frame_processors' in desc:
            return []
        return [k['id'] for k in desc['frame_processors']]


    def get_frame_processor_info(self, module, name):
        '''
        Retrieves the info of the given frame processor, if a matching id is found.

        Used for internal purpose. Performs a linear search.

        returns a tuple (key, value)
        Raises an exception if description file cannot be loaded.
        '''
        desc = self.load_description(module)
        if desc is None:
            raise Exception("Cannot load description file for"+module)

        if not 'frame_processors' in desc:
            return []
        result = [k for k in desc['frame_processors'] if k['id'] == name]
        if len(result) == 0:
            return None
        return result[0]


    def get_all_frame_processors(self):
        '''
        Returns a list of globally available frame processors
        '''
        fplist = []
        modules = self.list_all_modules()
        #fplist = [fplist+x for x in [self.get_frame_processors(m) for m in modules] ]

        for m in modules:
            fplist += self.get_frame_processors(m)
        return fplist


    def run_module(self, module, service, params=None):
        """
        Run a specific module.

        Parameters
        ----------
        module: string
           Name of the module.
        service: string
           Name of the service.
        params:
           Input parameters for the module.
        Image should be passed here.
        """
        pass


    def load_api(self, module, service):
        '''
        Loads the service as an API to be used by a python script
        '''
        info = self.load_service_description(module, service)
        if not 'api' in info:
            raise Exception("Service {}.{} does not support api calls".format(module, service))
            return
        info = info['api']
        apiname=module
        filename= os.path.splitext(info['file'])[0]
        classname="BaseRecognizerAPI"
        a = importlib.__import__(apiname+"."+filename, fromlist= [classname])
        return getattr(a, classname)


    def load_frame_processor(self, module, name):
        '''
        Loads a frame processor class to be used by a python script.
        '''
        info = self.get_frame_processor_info(module, name)

        apiname=module
        filename= os.path.splitext(info['file'])[0]
        classname=info['class']
        a = importlib.__import__("face_modules."+apiname+"."+filename, fromlist= [classname])
        return getattr(a, classname)



    def api_call(self, module, service, opts):
        '''
        Calls the default API method from a python script.

        To be used by a python script.
        '''
        api = self.load_api(module, service)
        info = self.load_service_description(module, service)

        if not 'api' in info:
            raise Exception("Service {}.{} does not support api calls".format(module, service))
            return
        if not 'method' in info['api']:
            raise Exception("{}/{}/module.json: 'method' not defined for the api call".format(module, service))
            return
        api_inst = api(default_config)
        methodname = info['api']['method']

        opts['meta'] = {"module": module, "service": service}
        method = getattr(api_inst, methodname)
        return method(opts)

#m = ModLoader()
##api = m.load_api("base", "train")

#inp = {"name": "hudata", "label": "folder", "output": "test1"}
#pred_inp = {"image": {"type":"file", "data": "/tmp/1.jpg"}, "model": "test1"}

#print("Input parameters:")
#print(inp)
#print("Training")
#ret = m.api_call("base", "train", inp)
#print("Training complete")

#ret = m.api_call("base", "predict", pred_inp)
#print(ret)
