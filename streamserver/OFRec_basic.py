import cv2
import openface
openfacedir = "/home/itadmin/Public/build/openface"
datasetdir = "/home/itadmin/Public/datasets"

#openfacedir = "/media/extra/build/openface"
#datasetdir = "/media/extra/datasets"

#https://stackoverflow.com/questions/6893968/how-to-get-the-return-value-from-a-thread-in-python

#from concurrent.futures import ProcessPoolExecutor
#import time
from frapi.dlibtools import DLIBDetector, DLIBAligner
#from frapi.dlibtools import CorrelationTracker as DLIBTracker

#from frapi.mtcnn import MTCNNDetector

import dlib
#from common.sort import Sort
from OpenFaceRecognizer import OpenFaceRecognizer as OFP
#import uuid

model_path="/home/itadmin/Public"

import pymongo
client = pymongo.MongoClient('mongodb://127.0.0.1:27017/hufru')
db = client.hufru


#model_path="/media/extra/models"
class FrameProcessor():
    def __init__(self):
        self.detector = DLIBDetector()
        self.detector.threshold = -1
        self.aligner = DLIBAligner()
#        self.rec = OFP(model_path+"/models/classifiers/huit/classifier.pkl")
        self.rec = OFP("/tmp/featureDir/classifier.pkl")

        self.rec.load_names(model_path+"/models/classifiers/huit/names.csv")

        self.last_align = None
        self.last_label = ""
        self.last_confidence = 0
        self.align_future = None
        self.predict_future = None
        self.lose_track = 1 #lose confidence track after k frame
        self.missed_frames = 0

    def drawBorder(self, img, r):
        cv2.rectangle(img, (r.left(), r.top()), (r.right(), r.bottom()), (255,0,0), 2)
    def drawRect(self, img, r):
        x1,y1,x2,y2 = r
        cv2.rectangle(img, (x1,y1), (x2,y2), (255,0,0), 2)

    def drawText(self, img, pos, text):
        cv2.putText(img, text, (pos[0]+5,pos[1]+5), cv2.FONT_HERSHEY_PLAIN, 1.0, (128,76, 250), 2)

    def process_image(self, in_img):
        out_img = in_img.copy()
        rgb_img = cv2.cvtColor(in_img, cv2.COLOR_BGR2RGB)
        gray_img = cv2.cvtColor(in_img, cv2.COLOR_BGR2GRAY)
        _w = in_img.shape[1]
        _h = in_img.shape[0]

        small_img = cv2.resize(rgb_img, (int(_w/2), int(_h/2)))

        #rects = self.detector.detector(in_img, 0)
        rect = self.aligner.aligner.getLargestFaceBoundingBox(small_img)

        if rect is None:
            self.missed_frames += 1
            if self.missed_frames > self.lose_track:
                self.missed_frames = 0
                self.last_label = ""
                self.last_confidence = 0
            return out_img

        r = rect
        k = 2
        x1,y1,x2,y2 = int(r.left()*k), int(r.top()*k), int(r.right()*k), int(r.bottom()*k)
        #align = self.aligner.align(face)

        self.drawRect(out_img, (x1,y1,x2,y2) )

        #if align is not None:
        #    result = self.rec.predict(align)
        #    label = result[0].decode()
        #    if result[1] < 0.3:
        #        label = "Unknown"
        #        self.drawText(out_img, (x1,y1-5), label)
        #        self.drawText(out_img, (x1,y2+5), "'{0:.2f}'".format(result[1]))

        label = "Unknown"
        db.screeninfo.update({"type": "face"}, {'$set': {'label': "Unknown"}}, upsert=True)
        if self.last_confidence >= 0.16:
            label = self.rec.personToName(self.last_label)
            #label = self.last_label
            db.screeninfo.update({"type": "face"}, {'$set': {'label': label}}, upsert=True)
            print("Name saved")

        self.drawText(out_img, (x1,y1-5), label)
        self.drawText(out_img, (x1,y2+5), "'{0:.2f}'".format(self.last_confidence))


        face = in_img[y1:y2, x1:x2]
        if self.align_future is None:
            self.align_future = self.aligner.align_single_async(face)
        else:
            if self.align_future.done():
                self.lastAlign = self.align_future.result()
                self.align_future = None

        if self.lastAlign is not None:
            if self.predict_future is None:
                self.predict_future = self.rec.predict_async(self.lastAlign)
                self.lastAlign = None


        if not (self.predict_future is None):
            if self.predict_future.done():
                res = self.predict_future.result()
                if self.last_confidence < res[1]:
                    self.last_label = res[0].decode()
                    self.last_confidence = res[1]
                self.predict_future = None


        #if self.predict_future = None:
        #    self.predict_future = self.aligner.predict(rgb_img, self.async_results)


        return out_img

