#!/usr/bin/python3
import base64
import cv2
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('dev', type=str,
                   help='Media device to take input from')

args = parser.parse_args()
vidstream = cv2.VideoCapture(args.dev);

ret, _ = vidstream.read()

if not ret:
    print("Cannot read from input stream device")
    exit()

import numpy as np
import websocket
import PIL.Image as Image
import atexit
import io
import tornado.web
import tornado.websocket
from tornado.websocket import websocket_connect
from tornado.ioloop import IOLoop, PeriodicCallback
#from ForwardStream2 import WebSocket
import threading
#import common.inception_resnet_v1

#from OFRec_local import FrameProcessor
from OFRec_basic import FrameProcessor


from SimpleWebSocketServer import SimpleWebSocketServer, WebSocket



#pi_ip = "10.125.192.10"



import signal
import sys
def signal_handler(sig, frame):
        print('You pressed Ctrl+C!')
        sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)
#signal.pause()


try:
    import thread
except ImportError:
    import _thread as thread
import time



from io import StringIO, BytesIO




fp = FrameProcessor()
class WebSocket(tornado.websocket.WebSocketHandler):
    def on_open(self):
        print("Starting out socket")

    def on_close(self):
        pass

    def on_message(self, message):
        if message == "read_camera":
            print("Reading from in stream")
            self.camera_loop = PeriodicCallback(self.loop, 10)
            self.camera_loop.start()
        else:
            print("Unsupported function: "+message)

    def setSharedVar(self, sv):
        self.shared_vars = sv[0]

    def loop(self):
        sio = io.BytesIO()

        read, img = vidstream.read()
        if not read:
            raise ValueError("Cannot read from stream device, "+pi_ip)

        img = fp.process_image(img)

        cv2_im = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

        img = Image.fromarray(cv2_im)
        img.save(sio, "JPEG")

        try:
            self.write_message(base64.b64encode(sio.getvalue()))
        except tornado.websocket.WebSocketClosedError:
            print("Error occured")
            self.camera_loop.stop()

    def check_origin(self, origin):
        return True


img = None


def quit_gracefully():
    print("Closing websocket...")
    #ws.close()

atexit.register(quit_gracefully)

def on_message(ws, m):
    #img =  readb64(m)
    _, img = vidstream.read()

def on_error(ws, error):
    print(error)


def on_close(ws):
    print("### closed ###")

def on_open(ws):
    def run(*args):
        ws.send("read_camera")
    thread.start_new_thread(run, ())


class OutWSServer(WebSocket):
    def handleMessage(self):
        # echo message back to client
        print(self.data)
        self.sendMessage("AOK")
        #self.sendMessage(self.data)

    def handleConnected(self):
        print(self.address, 'connected')

    def handleClose(self):
        print(self.address, 'closed')


#https://stackoverflow.com/questions/33754935/read-a-base-64-encoded-image-from-memory-using-opencv-python-library
def readb64(base64_string):
    sbuf = BytesIO()
    sbuf.write(base64.b64decode(base64_string))
    pimg = Image.open(sbuf)
    return cv2.cvtColor(np.array(pimg), cv2.COLOR_RGB2BGR)


if __name__ == "__main__":
    port = 4001
    handlers = [(r"/websocket", WebSocket)]
    application = tornado.web.Application(handlers)
    application.listen(port)
    print("Starting out socket")
    tornado.ioloop.IOLoop.instance().start()


    #t1.daemon = True
    #t1.start()

    #t2.setDaemon(True)
    #t2.start()

    #dev_id =

    #print("Starting in socket")
    #ws.run_forever()

    #ws.



#import cv2
#import time

vc = cv2.VideoCapture("http://10.125.192.232:8090/?action=stream");
#vc = cv2.VideoCapture("udp://10.125.192.232:8090");
#vc = cv2.VideoCapture("http://10.125.192.232:8090/frame.mjpg")

#bytes=''
#while True:
#    _, data = vc.read()


#    cv2.imshow('img', data)

#    if cv2.waitKey(1) & 0xFF == ord('q'):
#        break
    #time.sleep(0.01)


#cs.recvfrom(
#cv2.destroyAllWindows()
