#import numpy as np
import pickle
from OpenFaceRecognizer import OpenFaceRecognizer

of = OpenFaceRecognizer("/media/extra/models/classifiers/huit/classifier.pkl")
embeddings = of.loadEmbeddings("/tmp/aligned1/", False)

with open('/tmp/embeddings.pkl', 'wb') as handle:
    pickle.dump(embeddings, handle, protocol=pickle.HIGHEST_PROTOCOL)

#of.load_names("/tmp/aligned1/names.csv")

