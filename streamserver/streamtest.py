import base64
import cv2
import numpy as np
import websocket
import PIL.Image as Image
import atexit

try:
    import thread
except ImportError:
    import _thread as thread
import time

from io import StringIO, BytesIO

img = None


def quit_gracefully():
    print("Closing websocket...")
    ws.close()
    

atexit.register(quit_gracefully)

def on_message(ws, m):
    img =  readb64(m)

def on_error(ws, error):
    print(error)


def on_close(ws):
    print("### closed ###")

def on_open(ws):
    def run(*args):
        ws.send("read_camera")

    thread.start_new_thread(run, ())


#https://stackoverflow.com/questions/33754935/read-a-base-64-encoded-image-from-memory-using-opencv-python-library
def readb64(base64_string):
    sbuf = BytesIO()
    sbuf.write(base64.b64decode(base64_string))
    pimg = Image.open(sbuf)
    return cv2.cvtColor(np.array(pimg), cv2.COLOR_RGB2BGR)

if __name__ == "__main__":
    websocket.enableTrace(True)
    ws = websocket.WebSocketApp("ws://10.125.46.86:4001/websocket",
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close)
    ws.on_open = on_open
    ws.run_forever()

