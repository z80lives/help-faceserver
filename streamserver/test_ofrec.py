#import numpy as np
import cv2

from OFRec2 import FrameProcessor
#from OFRec_basic import FrameProcessor


import argparse

parser = argparse.ArgumentParser()
parser.add_argument('dev', type=str,
                   help='Media device to take input from')

parser.add_argument('--model', type=str,
                    help="pkl Classifier Model")

args = parser.parse_args()

fp = FrameProcessor(model=args.model)
#videostream = cv2.VideoCapture(0)
videostream = cv2.VideoCapture(args.dev)
#videostream = cv2.VideoCapture("/media/extra/datasets/sample/ferrell.mp4")

while True:
    _, img = videostream.read()
    img = fp.process_image(img)

    cv2.imshow("test", img)
    if cv2.waitKey(1) == ord('q'):
        break

cv2.destroyAllWindows()
