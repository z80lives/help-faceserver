from keras.models import load_model
import numpy as np
import cv2

class EmotionRecognizer():
    def __init__(self):
        self.model_path = './models/emotion_models/fer2013_mini_XCEPTION.102-0.66.hdf5'
        self.labels = {0:'angry',1:'disgust',2:'fear',3:'happy',
                       4:'sad',5:'surprise',6:'neutral'}
        self.offsets = (20, 40)
        self.classifier = load_model(self.model_path, compile=False)
        self.target_size = self.classifier.input_shape[1:3]
        self.window = []

    def getOffsets(self):
        return self.offsets

    def getTargetSize(self):
        return self.target_size
    
    def process_face(self, gray_face):
        try:
            gray = cv2.resize(gray_face, (self.target_size))
        except:
            return None
        gray = Utils.preprocess_input(gray, False)
        gray = np.expand_dims(gray, 0)
        gray = np.expand_dims(gray, -1)
        return gray


    def predict(self, gray_face):
        p = self.classifier.predict(gray_face)
        result = np.argmax(p)
        return self.labels[result]

class GenderRecognizer():
    def __init__(self):
        self.model_path = './models/gender_models/simple_CNN.81-0.96.hdf5'
        self.labels =   {0:'Female', 1:'Male'}
        self.offsets = (30, 60)
        self.classifier = load_model(self.model_path, compile=False)
        self.target_size = self.classifier.input_shape[1:3]
        self.window = []

    def predict(self, color_face):
        predictions = self.classifier.predict(color_face)
        predicted_index = np.argmax(predictions)
        label = self.labels[predicted_index]
        return label
        
    def process_face(self, cface):
        try:
            face = cv2.resize(cface, (self.target_size))
        except:
            return None
        face = np.expand_dims(face, 0)
        face = Utils.preprocess_input(face, False)
        return face
        
    def getOffsets(self):
        return self.offsets

    def getTargetSize(self):
        return self.target_size
    


class Utils():
    def preprocess_input(x, v2=True):
        x = x.astype('float32')
        x = x / 255.0
        if v2:
            x = x - 0.5
            x = x * 2.0
        return x

    def apply_offsets(r, offsets):
        #x, y, width, height = face_coordinates
        x,y, width, height = r.left(), r.top(), r.width(), r.height()
        x_off, y_off = offsets
        return (x - x_off, x + width + x_off, y - y_off, y + height + y_off)

    def _imread(image_name):
        return imread(image_name)

    def _imresize(image_array, size):
        return imresize(image_array, size)

    def to_categorical(integer_classes, num_classes=2):
        integer_classes = np.asarray(integer_classes, dtype='int')
        num_samples = integer_classes.shape[0]
        categorical = np.zeros((num_samples, num_classes))
        categorical[np.arange(num_samples), integer_classes] = 1
        return categorical


