'''
Capture face
'''
import numpy as np
import cv2
from Recognizer import EmotionRecognizer, GenderRecognizer, Utils
import dlib
import openface

from multiprocessing.dummy import Pool
import time


face_rects = []
yolo_rects = []
thread1State= False
thread2State = False
yolo_updated = False
dlib_updated = False
shared_vars = [face_rects, thread1State, thread2State, yolo_rects, yolo_updated, dlib_updated]

def drawBorder(img, r, col=(255,0,0)):
    cv2.rectangle(img, (r.left(), r.top()), (r.right(), r.bottom()), col, 2)

def drawTextRect(img, pos, txt,  col=(255,255,0), scale=1, bottom=False):
    if isinstance(pos, dlib.rectangle):
        if bottom==False:
            pos = pos.left()+5, pos.top()-5
        else:
            pos = pos.left()+5, pos.bottom()+10
    cv2.putText(img, txt, pos, cv2.FONT_HERSHEY_SIMPLEX, scale, col)


def dlib_detect(args):
    shared_vars[1] = True
    detector, frame, arg = args
    start_time = time.time()
    ret = detector(frame, arg)
    print(ret)
    end_time = time.time() - start_time
    print("DLIB Time took = {}".format(end_time))
    return ret

def _dlibfacedone(res):
    shared_vars[0] = res
    shared_vars[1] = False
    shared_vars[5] = True



print("Loading emotion recognizer..")
em = EmotionRecognizer()
gm = GenderRecognizer()

stream = cv2.VideoCapture("/home/itadmin/Public/datasets/sample/community2.mp4")
for i in range(100):
    stream.read()


detector = dlib.get_frontal_face_detector()

align = openface.AlignDlib("/home/itadmin/build/openface/models/dlib/shape_predictor_68_face_landmarks.dat")


pool = Pool(processes=1)

start_time = time.time()
old_faces = []
time_passed = -1
while True:
    ret, frame = stream.read()
    if not ret:
        print("Cannot read media device")
        break

    frame = cv2.resize(frame, (640, 480))
    imsmall = cv2.resize(frame, (640, 480))
    frame_h = frame.shape[0]                        
    frame_w = frame.shape[1]
    buff_h =  imsmall.shape[0]                        
    buff_w = imsmall.shape[1]

    #Start detection thread if possible
    
    if time_passed > 1 or time_passed == -1:
        if shared_vars[1] == False:
            start_time = time.time()
            pool.apply_async(dlib_detect, [[detector, imsmall, 0]], callback=_dlibfacedone)
    time_passed = time.time() - start_time


            
    rgb_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)


    #face_rects = detector(frame, 1)
    face_rects = shared_vars[0]

    yolo_updated = shared_vars[4]
    dlib_updated = shared_vars[5]
    if (len(old_faces) < len(face_rects)) or (dlib_updated and len(face_rects)>0):
        shared_vars[4] = False
        shared_vars[5] = False
        old_faces = []
        for face in face_rects:
            tracker = dlib.correlation_tracker()
            tracker.start_track(imsmall, face)
            old_faces.append(tracker)
    else:
        
    #for rect in face_rects:
        for i, tracker in enumerate(old_faces):
            quality = tracker.update(frame)
            if quality > 5:
                pos = tracker.get_position()                

                scalew = frame_w/buff_w
                scaleh = frame_h/buff_h
                rect = dlib.rectangle( int(pos.left()*scalew), int(pos.top()*scaleh), int(pos.right()*scalew), int(pos.bottom()*scaleh))
                x1, x2, y1, y2 = Utils.apply_offsets(rect, em.getOffsets())
                gray = gray_frame[y1:y2, x1:x2]

                x1, x2, y1, y2 = Utils.apply_offsets(rect, gm.getOffsets())
                colorFace = rgb_frame[y1:y2, x1:x2]        

                #print(colorFace.shape)
                #print(colorFace)
                #cv2.imshow('align', rgb_frame[y1:y2,x1:x2])
                #dbg = frame[rect.top(): rect.bottom(), rect.left():rect.right()]
                #cv2.imshow('align', dbg)
                #cv2.moveWindow('align',10,10)
                
                drawBorder(frame, rect)
        
                gray = em.process_face(gray)
                colorFace = gm.process_face(colorFace)
                if colorFace is None or gray is None:
                    continue

        
                #cv2.imshow('align', gray)
                #cv2.moveWindow('align',10,10)

        
                emotion_res = em.predict(gray)
                gender_res = gm.predict(colorFace)
        

                #alignedFace = align.align(96, frame, rect, landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)

                drawTextRect(frame, rect, gender_res)
                drawTextRect(frame, rect, emotion_res, bottom=True)
            else:
                old_faces.pop(i)

    cv2.imshow('out', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break



stream.release()
cv2.destroyAllWindows()

