'''
Capture face
'''

import numpy as np
import cv2
from Recognizer import EmotionRecognizer, GenderRecognizer, Utils
import dlib
import openface


def drawBorder(img, r, col=(255,0,0)):
    cv2.rectangle(img, (r.left(), r.top()), (r.right(), r.bottom()), col, 2)

def drawTextRect(img, pos, txt,  col=(255,255,0), scale=1, bottom=False):
    if isinstance(pos, dlib.rectangle):
        if bottom==False:
            pos = pos.left()+5, pos.top()-5
        else:
            pos = pos.left()+5, pos.bottom()+10
    cv2.putText(img, txt, pos, cv2.FONT_HERSHEY_SIMPLEX, scale, col)

print("Loading emotion recognizer..")
em = EmotionRecognizer()
gm = GenderRecognizer()

stream = cv2.VideoCapture("/home/itadmin/Public/datasets/sample/court.mp4")
for i in range(100):
    stream.read()


detector = dlib.get_frontal_face_detector()

align = openface.AlignDlib("/home/itadmin/build/openface/models/dlib/shape_predictor_68_face_landmarks.dat")


while True:
    ret, frame = stream.read()
    if not ret:
        print("Cannot read media device")
        break

    frame = cv2.resize(frame, (448, 224))
    face_rects = detector(frame, 1)

    rgb_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    
    for rect in face_rects:
        x1, x2, y1, y2 = Utils.apply_offsets(rect, em.getOffsets())
        gray = gray_frame[y1:y2, x1:x2]

        x1, x2, y1, y2 = Utils.apply_offsets(rect, gm.getOffsets())
        colorFace = rgb_frame[y1:y2, x1:x2]        


        cv2.imshow('align', gray)
        cv2.moveWindow('align',10,10)

        
        gray = em.process_face(gray)
        colorFace = gm.process_face(colorFace)
        if colorFace is None or gray is None:
            continue
        
        emotion_res = em.predict(gray)
        gender_res = gm.predict(colorFace)
        

        #alignedFace = align.align(96, frame, rect, landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)

        drawTextRect(frame, rect, gender_res)
        drawTextRect(frame, rect, emotion_res, bottom=True)
        drawBorder(frame, rect)

    cv2.imshow('out', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break


stream.release()
cv2.destroyAllWindows()

