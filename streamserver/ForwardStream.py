import base64
import cv2
import numpy as np
import io
import tornado.web
import tornado.websocket
from tornado.websocket import websocket_connect
from tornado.ioloop import PeriodicCallback
from PIL import Image

import shared_vars as sv

# class CVImg():
#     def __init__(self):        
#         self.img = np.zeros((480, 640, 3), np.uint8)

#     def getImage(self):
#         return self.img
#     def setImage(self, img):
#         self.img = img
    
# cvimg = CVImg()
def drawBorder(img, r):
    cv2.rectangle(img, (r.left(), r.top()), (r.right(), r.bottom()), (255,0,0), 2)

import dlib
detector = dlib.get_frontal_face_detector()

class WebSocket(tornado.websocket.WebSocketHandler):
    def on_open(self):
        #self.shared_vars = sv.shared_vars
        print("Starting out socket")
        self.old_faces = []
        self.faces = []
        
    def on_close(self):
        pass
        #self.inws.close()
        
    def on_message(self, message):
        if message == "read_camera":
            print("Reading from in stream")
            self.camera_loop = PeriodicCallback(self.loop, 10)
            self.old_faces = []
            self.faces = []
            self.camera_loop.start()
        else:
            print("Unsupported function: "+message)
            
    def loop(self):
        sio = io.BytesIO()
        #img = cvimg.getImage()
        #img = sv.shared_vars[0]

        #img = Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
        #img = np.zeros((480, 640, 3), np.uint8)
        img = sv.shared_vars[3]

        face_rects = detector(img, 0)
        #face_rects = []
        if len(self.old_faces) < len(face_rects):
            self.old_faces = []
            for face in face_rects:
                tracker = dlib.correlation_tracker()
                tracker.start_track(img, face)
                self.old_faces.append(tracker)
        else:
            for i, tracker in enumerate(self.old_faces):
                quality = tracker.update(img)
                if quality > 5:
                    pos = tracker.get_position()
                    pos = dlib.rectangle( int(pos.left()),
                                          int(pos.top()),
                                          int(pos.right()),
                                          int(pos.bottom()) )
                    drawBorder(img, pos)
                else:
                    self.old_faces.pop(i)
                
        #for f in face_rects:
        #    drawBorder(img, f)

        
        cv2_im = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

        img = Image.fromarray(cv2_im)        
        img.save(sio, "JPEG")
        
        try:
            self.write_message(base64.b64encode(sio.getvalue()))
        except tornado.websocket.WebSocketClosedError:
            print("Error occured")
            self.camera_loop.stop()

    def check_origin(self, origin):
        return True

class CamInSocket(tornado.websocket.WebSocketHandler):
    def on_message(self, message):
        print("Message")
    
    def on_close():
        print("Camera Feed socket closed") 
