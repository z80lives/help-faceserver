import base64
import cv2

import numpy as np
import websocket
import PIL.Image as Image
import atexit
import io
import tornado.web
import tornado.websocket
from tornado.websocket import websocket_connect
from tornado.ioloop import PeriodicCallback
from ForwardStream import WebSocket
import threading

import shared_vars as sv
sv.init()

shared_vars = sv.shared_vars


import signal
import sys
def signal_handler(sig, frame):
        print('You pressed Ctrl+C!')
        shared_vars[0].stop()
        shared_vars[1].stop()
        sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)
#signal.pause()


try:
    import thread
except ImportError:
    import _thread as thread
import time

from io import StringIO, BytesIO

img = None


def quit_gracefully():
    shared_vars[0].stop()
    shared_vars[1].stop()
    print("Closing websocket...")
    #ws.close()
    

atexit.register(quit_gracefully)

def on_message(ws, m):
    img =  readb64(m)
    shared_vars[3] = img
    
def on_error(ws, error):
    print(error)


def on_close(ws):
    print("### closed ###")

def on_open(ws):    
    def run(*args):
        ws.send("read_camera")
    thread.start_new_thread(run, ())


#https://stackoverflow.com/questions/33754935/read-a-base-64-encoded-image-from-memory-using-opencv-python-library
def readb64(base64_string):
    sbuf = BytesIO()
    sbuf.write(base64.b64decode(base64_string))
    pimg = Image.open(sbuf)
    return cv2.cvtColor(np.array(pimg), cv2.COLOR_RGB2BGR)


class T1(threading.Thread):
    def run(self):
        port = 4001       
        handlers = [(r"/websocket", WebSocket)]
        application = tornado.web.Application(handlers)
        application.listen(port)
        print("Starting out socket")
        tornado.ioloop.IOLoop.instance().start()
    def stop(self):
        self.running = False
 
class T2(threading.Thread):
    def run(self):
        ws = websocket.WebSocketApp("ws://10.125.196.131:8000/websocket",
                                    on_message = on_message,
                                    on_error = on_error,
                                    on_close = on_close)
        ws.on_open = on_open
        print("Starting in socket")
        ws.run_forever()
        
    def stop(self):
        self.running = False

if __name__ == "__main__":
    #websocket.enableTrace(True)
    #ws = websocket.WebSocketApp("ws://10.125.196.131:8000/websocket",
    #                          on_message = on_message,
    #                          on_error = on_error,
    #                          on_close = on_close)
    #ws.on_open = on_open
    t1 = T1()
    t2 = T2()
    shared_vars[0] = t1
    shared_vars[1] = t2
    shared_vars[2] = False #quit flag

    
    t1.daemon = True    
    t1.start()
    
    t2.setDaemon(True)
    t2.start()

    while True:
            #print(shared_vars[3])
            pass
            
    #dev_id = 

    #print("Starting in socket")
    #ws.run_forever()

    #ws.


