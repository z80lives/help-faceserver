import numpy as np
import cv2
from sklearn.base import BaseEstimator
#from sklearn import cross_validation as cval
from sklearn.model_selection import cross_val_score as cval
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import  precision_score, make_scorer
#from sklearn.model_selection import precision_score
from read_images import read_images

class FaceRecognizerModel(BaseEstimator):
    def __init__(self):
        self.model = cv2.face.EigenFaceRecognizer_create()

    def fit(self, X, y):
        self.model.train(X, y);

    def predict(self, T):
        T = np.asarray(T)
        a = []
        for i in range(0, T.shape[0]):
            res,_ = self.model.predict(T[i])
            a.append(res)
        return np.asarray(a)
        #return [self.model.predict(T[i]) for i in range(0, T.shape[0])]


[X,y] = read_images("/media/extra/datasets/att_faces", (32,32), 'gray')
y = np.asarray(y, dtype=np.int32)
cv = StratifiedKFold(10)
estimator = FaceRecognizerModel()

#help(cval.cross_val_score)
precision_scores = cval(estimator, X, y, scoring="accuracy", cv=cv)

print(precision_scores)
