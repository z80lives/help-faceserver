import cv2
import openface
import dlib

import pickle
import pandas


def train(workDir = "./media/extra/datasets/"):
    print("Loading embeddings")
    fname = "{}./labels.csv".format(workDir)
    labels = pd.read_csv(fname, header=None).as_matrix()
    
