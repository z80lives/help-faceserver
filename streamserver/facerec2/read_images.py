import os
import sys
import cv2
import numpy as np

#as written by Philipp Wagner(bytefish)
#taken from: https://stackoverflow.com/questions/12197383/face-recognition-in-opencv-python-far-frr/12324991#12324991
def read_images(path, sz=None, col='rgb'):
    c = 0
    X, y = [], []
    for dirname, dirnames, filenames in os.walk(path):
        #print(filenames)
        for subdirname in dirnames:
            subject_path = os.path.join(dirname, subdirname)
            for filename in os.listdir(subject_path):
                try:
                    im = cv2.imread(os.path.join(subject_path, filename), cv2.IMREAD_GRAYSCALE)
                    if (sz is not None):
                        im = cv2.resize(im, sz)
                    #if (col == 'gray'):
                    #    im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
                    X.append(np.array(im, dtype=np.uint8))
                    y.append(c)
                except IOError, (errno, strerror):
                    print("I/O error({0}): {1}".format(errno, strerror))
                except:
                    print("Unexpected error:")
                    raise
            c = c + 1
    return [X, y]

#X,y = read_images("/media/extra/datasets/hudata")

