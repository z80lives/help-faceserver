import os
import numpy as np
import PIL.Image as Image

def read_images(path, sz=None):
    c = 0
    X, y = [], []
    for dirname, dirnames, filenames in os.walk(path):
        for subdirname in dirnames:
            subject_path = os.path.join(dirname, subdirname)
            for filename in os.listdir(subject_path):
                im = Image.open(os.path.join(subject_path, filename))
                #im = im.convert("L")
                X.append(np.asarray(im, dtype=np.uint8))
                c = (int(subdirname[1:]))
                y.append(c)
            #c = c + 1
    return [X, y]


def asRowMatrix(X):
    if len(X) == 0:
        return np.array([])
    mat = np.empty( (0, X[0].size), dtype=X[0].dtype)
    for row in X:
        mat = np.vstack( (mat, np.asarray(row).reshape(1,-1)) )
    return mat

def asColumnMatrix (X):
    if len (X) == 0:
        return np . array ([])
    mat = np . empty (( X[0].size , 0) , dtype = X [0]. dtype )
    for col in X:
        mat = np . hstack (( mat , np . asarray ( col ). reshape ( -1 ,1) ))
    return mat
