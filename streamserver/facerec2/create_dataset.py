import cv2
import numpy
import sys
import openface
import dlib
import os

cap = cv2.VideoCapture(1)
#cap = cv2.VideoCapture(0)
dir_ = "interndataset/"
def drawOutline(img, faces):
    for f in faces:
        cr = ((f.left(), f.top()), (f.right(), f.bottom()))
        cv2.rectangle(img, (f.left(), f.top()),(f.right(),f.bottom()),(255,0,0),2)

def saveImage(img, faces, face_id, count):
    print(count[0])
    for f in faces:
        x,y,w,h = (f.left(), f.top(), f.right(  ), f.bottom())
        #cv2.imwrite("dataset/User." + str(face_id) + '.' + str(count[0]) + '.jpg', img[y:h, x:w])
        pdir = "person-"+str(face_id)+"/"
        cv2.imwrite(dir_+pdir+"image-" + str(count[0]) + '.jpg', img[y:h, x:w])
        count[0] += 1


#face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
#eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')

print("Loading dlib align..")
align = openface.AlignDlib("/media/extra/build/openface/models/dlib/shape_predictor_68_face_landmarks.dat")

if len(sys.argv) != 2:
    print("Please provide the label argument")
    exit()

face_id = sys.argv[1]
count = [1]

font                   = cv2.FONT_HERSHEY_SIMPLEX
fontColor              = (255,255,255)

dataDir = dir_+"person-"+str(face_id)
if not os.path.isdir(dataDir):
    os.mkdir(dataDir)

while(True):
    ret, img = cap.read()
    img = cv2.flip(img ,1)
    #hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    #gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    #hsv[:,:,2] += 100
    #test = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
    #gray = cv2.cvtColor(test, cv2.COLOR_BGR2GRAY)

    bbs = align.getAllFaceBoundingBoxes(img)

    #for bb in bbs:
    #    alignedFace = align.align(96, img, bb, landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)
    ##for bb in bbs:

    key = cv2.waitKey(1)
    if key & 0xFF == ord('q'):
        break
    elif key & 0xFF == ord('s'):
        print("Saving image")
        if len(bbs) > 0:
            f = bbs[0]
            x,y,w,h = (f.left(), f.top(), f.right(  ), f.bottom())
            pdir = "person-"+str(face_id)+"/"
            cv2.imshow('out', img[y:h, x:w])
            cv2.moveWindow("out", 100,100)
            cv2.imwrite(dir_+pdir+"image-" + str(count[0]) + '.jpg', img[y:h, x:w])
            count[0] += 1

    drawOutline(img, bbs)

    #cv2.imshow('wcTest', test)
    #drawOutline(img, faces)
    state = "No face detected"
    if len(bbs) > 0:
        state = "Not aligned"

    cv2.putText(img, str(count[0]), (10,30), font, 1, fontColor)
    cv2.putText(img, state, (10,60), font, 1, fontColor)

    cv2.imshow('wcframe', img)
    #close on q


    if(count[0] > 10):
        break


cap.release()
cv2.destroyAllWindows()
