import base64
import cv2
import numpy as np
import websocket
import PIL.Image as Image

try:
    import thread
except ImportError:
    import _thread as thread
import time

from io import StringIO, BytesIO

img = None

def on_message(ws, m):
    img =  readb64(m)
    cv2.imshow('out', img)
    cv2.waitKey(1)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        ws.close()
    #print("AOK!")
    #print(message)

def on_error(ws, error):
    print(error)


def on_close(ws):
    print("### closed ###")
    ws.close()

def on_open(ws):

    def run(*args):
        ws.send("read_camera")

    thread.start_new_thread(run, ())


#https://stackoverflow.com/questions/33754935/read-a-base-64-encoded-image-from-memory-using-opencv-python-library
def readb64(base64_string):
    sbuf = BytesIO()
    sbuf.write(base64.b64decode(base64_string))
    pimg = Image.open(sbuf)
    return cv2.cvtColor(np.array(pimg), cv2.COLOR_RGB2BGR)

#ws = websocket.WebSocket()
#ws.connect("ws://10.125.195.42:8000/websocket")
#ws.send("read_camera")
#print("Sent")
#print("Receiving...")
#result =  ws.recv()
#img =  readb64(result)
#ws.close()

if __name__ == "__main__":
    websocket.enableTrace(True)
    ws = websocket.WebSocketApp("ws://10.125.46.86:4001/websocket",
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close)
    ws.on_open = on_open
    ws.run_forever()

#cv2.imshow('test', img)
#cv2.waitKey(0)
