import dlib
import cv2

#stream = cv2.VideoCapture(0)
stream = cv2.VideoCapture("/media/extra/datasets/sample/court.mp4")

detector = dlib.get_frontal_face_detector()

def drawBorder(img, r):
    cv2.rectangle(img, (r.left(), r.top()), (r.right(), r.bottom()), (255,0,0), 2)

def colorHistogramNormalize(imgIn):
    img_yuv = cv2.cvtColor(imgIn, cv2.COLOR_BGR2YUV)
    img_yuv[:,:,0] = cv2.equalizeHist( img_yuv[:,:,0] )
    return cv2.cvtColor(img_yuv, cv2.COLOR_YUV2BGR)


old_faces = []

k = 4
while True:
    ret, img = stream.read()
    if not ret:
        print("Cannot read from the video stream")
        break


    #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    frame = cv2.resize(img, (0, 0), fx=0.25, fy=0.25)
    _w = img.shape[1]
    _h = img.shape[0]
    #frame = cv2.resize(img, (_w/k, _h/k))
    #frame = cv2.resize(img, (320,200))
    #frame = colorHistogramNormalize(frame)
    #frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    #frame = img

    overlay = img.copy()
    output = img

    alpha = 0.5


    face_rects = detector(frame, 2)
    if len(old_faces) < len(face_rects):
        old_faces = []
        for face in face_rects:
            tracker = dlib.correlation_tracker()
            tracker.start_track(frame, face)
            old_faces.append(tracker)

    else:
        for i, tracker in enumerate(old_faces):
            quality = tracker.update(frame)
            if quality > 5:
                pos = tracker.get_position()
                pos = dlib.rectangle( k* int(pos.left()), k*int(pos.top()), int(pos.right()) * k, int(pos.bottom()) *k )
                drawBorder(output, pos)
            else:
                old_faces.pop(i)

    #print(face_rects)
    #    drawBorder(output,f)

    cv2.imshow("FrameOut", frame)
    cv2.moveWindow('FrameOut', 10, 10)
    cv2.imshow("Face Detection output", output)

    key = cv2.waitKey(1) & 0xFF

    if key == ord("q"):
        break


cv2.destroyAllWindows()
