import os

class LFWReader:
    def __init__(self, dir='/media/extra/datasets/lfw'):
        self.dir = dir
        self.classes = os.listdir(self.dir)
    def getPath(self, name):
        nm = '_'.join([x.title() for x in name.split(' ')])        
        return self.dir+'/'+nm
    def getFilename(self, name, num):
        nm = '_'.join([x.title() for x in name.split(' ')])        
        return self.dir+'/'+nm+'/'+nm+'_'+format(num, '04') +'.jpg'        
    def getClasses(self):
        return self.classes
    def countImages(self, className):
        return[f for f in os.listdir(self.getPath(className)) if re.match(r'[0-9]+.*\.jpg', f)]
    
lfwr = LFWReader()
fname = lfwr.getFilename("Fred Thompson", 1)
print(fname)
print(lfwr.countImages("Fred Thompson"))
#print(lfwr.getClasses())
