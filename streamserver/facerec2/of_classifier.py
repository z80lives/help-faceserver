import cv2
import numpy as np
import openface
import dlib
import math
import sys
import pickle 

#cap = cv2.VideoCapture(1)
#cap = cv2.VideoCapture("http://10.125.193.157:8080/video")
#cap = cv2.VideoCapture("/media/extra/datasets/sample/walking_crowd.mp4")
cap = cv2.VideoCapture("/home/itadmin/Public/datasets/sample/court.mp4")
#cap = cv2.VideoCapture("/media/extra/movies/Wonder Woman (2017) [YTS.AG]/Wonder.Woman.2017.720p.BluRay.x264-[YTS.AG].mp4")

def drawOutline(img, faces):
    for f in faces:
        cr = ((f.left(), f.top()), (f.right(), f.bottom()))
        cv2.rectangle(img, (f.left(), f.top()),(f.right(),f.bottom()),(255,0,0),2)

def drawOutlineWithText(img, faces, txt):
    for f in faces:
        cr = ((f.left(), f.top()), (f.right(), f.bottom()))
        cv2.rectangle(img, (f.left(), f.top()),(f.right(),f.bottom()),(255,0,0),2)

        font                   = cv2.FONT_HERSHEY_SIMPLEX
        bottomLeftCornerOfText = (10,500)
        fontScale              = 1
        fontColor              = (255,255,255)
        lineType               = 2

        cv2.putText(img, txt, (f.right()+5,f.top()+5), font, 1, fontColor)


def scaleBoundingBox(r, val):
    return dlib.rectangle(r.left()*val, r.top()*val, r.right()*val, r.bottom()*val)

def infer(rep):
    try:
        rep = rep.reshape(1, -1)
    except:
        print ("No Face detected")
        return (None, None)
    predictions = clf.predict_proba(rep).ravel()
    maxI = np.argmax(predictions)
    return (le.inverse_transform(maxI), predictions[maxI])


print("Loading dlib align..")
align = openface.AlignDlib("/home/itadmin/build/openface/models/dlib/shape_predictor_68_face_landmarks.dat")
print("Loading openface torch neural net..")
net = openface.TorchNeuralNet("/home/itadmin/build/openface/models/openface/nn4.small2.v1.t7", 96)

with open("./featureDir/classifier.pkl", 'rb') as f:
    try:
        (le, clf) = pickle.load(f, encoding='latin1')  # le - label and clf - classifer
    except:
        (le, clf) = pickle.load(f)   

labels = ["Unknown", "Kevin", "Ibrahim", "Rahimi"]
while(True):
    ret, img = cap.read()
    if not ret:
        print("Cannot read the media stream")
        break
    #img =
    #img = cv2.flip(img, 1)
    im_small = cv2.resize(img, (0,0), fx=.25, fy=0.25)
    #im_small = cv2.resize(img, (320,200))

    #histogram equalization
    img_yuv = cv2.cvtColor(im_small, cv2.COLOR_BGR2YUV)
    img_yuv[:,:,0] = cv2.equalizeHist( img_yuv[:,:,0] )
    im_small = cv2.cvtColor(img_yuv, cv2.COLOR_YUV2BGR)

    alignedFace = img

    #bbs = align.getAllFaceBoundingBoxes(im_small)
    bbs  = []
    bbs = align.getAllFaceBoundingBoxes(img)

    
    for bb in bbs:
        bb2 = scaleBoundingBox(bb, 4)
        alignedFace = align.align(96, im_small, bb2, landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)
        rep = net.forward(alignedFace)
        drawOutline(img, [bb])

        res = infer(rep)

        if res[0] != None:
            lno = int(res[0][7:])
        else:
            lno = 0
            
        confidence = res[1]
        if confidence < 0.5:
            lno = 0

        label=labels[lno] + ", c="+str(math.ceil(confidence*100))+"%"
        
        #print(predictions)
        #maxI = np.argmax(predictions)

        #distance = 0
        #if 'oldrep' in dir():
        #    d = oldrep - rep
        #    distance = np.dot(d,d)
        #oldrep = rep

        drawOutlineWithText(img, [bb2], label)

        cv2.imshow('aligned', alignedFace)
        cv2.moveWindow('aligned', 800, 10)


    #cv2.imshow('aligned', alignedFace)
    cv2.imshow('out', img)
    cv2.moveWindow("out", 320,200)
    cv2.imshow('out2', im_small)
    cv2.moveWindow('out2', 10, 10)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break


cap.release()
cv2.destroyAllWindows()
#bb = align.getLargestFaceBoundingBox(img)
#alignedFace = align.align(args.imgDim, img, bb, landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)

#rep1 = net.forward(alignedFace)
#d = rep1 - rep2
#distance = np.dot(d,d)
