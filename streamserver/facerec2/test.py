import numpy as np
from util import read_images, asRowMatrix
from eigen import pca, normalize

import  matplotlib.pyplot as plt
import matplotlib.cm as cm

def subplot(title, images, rows, cols, sptitle="subplot", sptitles=[], colormap=cm.gray, ticks_visible=True, filename=None):
    fig = plt.figure()
    fig.text(.5, .95, title, horizontalalignment='center')
    for i in range(len(images)):
        ax0 = fig.add_subplot(rows, cols, (i+1))
        plt.setp(ax0.get_xticklabels(), visible=False)
        plt.setp(ax0.get_yticklabels(), visible=False)
        plt.imshow(np.asarray(images[i]), cmap=colormap)
    plt.show()

X, y = read_images("/media/extra/datasets/att_faces2")

#print(X[0])
#X = X[1:17]
[D, W, mu] = pca(asRowMatrix(X), y)

E = []
for i in range(min(len(X), 16)):
    e = W[:,i].reshape(X[0].shape)
    #E.append(normalize(e, 0,255))
    E.append(e)

subplot(title="Eigenface for AT&T", images=E, rows=4, cols=4, sptitle="Eigenface", colormap=cm.gray)
#print(D, W, mu)
