import numpy as np

def pca(X, y, num_components=0):
    [n,d] = X.shape
    if (num_components <= 0) or (num_components > n):
        num_components = n
    mu = X.mean(axis=0)

    if n > d:
        C = np.dot(X.T, X)
        [l, ev] = np.linalg.eigh(c)
    else:
        C = np.dot(X, X.T)
        [l, ev] = np.linalg.eigh(C)
        ev = np.dot(X.T, ev)
        for i in range(n):
            ev[:, i] = ev[:,i]/np.linalg.norm(ev[:,i])
    idx = np.argsort(-l)
    l = l[idx]
    ev = ev[:, idx]
    l = l[0:num_components].copy()
    ev = ev[:,0:num_components].copy()
    return [l, ev, mu]


def project(W, X, mu=None):
    if mu is None:
        return np.dot(X,W)
    return np.dot(X-mu, W)

def reconstruct(W, Y, mu=None):
    if mu is None:
        return np.dot(Y, W.T)

def normalize(X, low, high, dtype=None):
    X = np.asarray(X)
    minX, maxX = np.min(X), np.max(X)
    X = X - float(minX)
    X = X / float((maxX - minX))
    X = X * (high-low)
    X = X + low
    if dtype is None:
        return np.asarray(X)
    return np.asarray(X, dtype=dtype)

