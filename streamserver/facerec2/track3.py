import numpy as np
import cv2
import dlib

#camera = cv2.VideoCapture(0)
camera = cv2.VideoCapture("/media/extra/datasets/sample/court.mp4")

def getOutputsNames(net):
    layersNames = net.getLayerNames()
    return [layersNames[i[0] - 1] for i in net.getUnconnectedOutLayers()]

def postProcess(img, outs):
    boxes = []
    frameWidth = img.shape[1]
    frameHeight = img.shape[0]
    if lastLayer.type == 'Region':
        for out in outs:
            for detection in out:
                scores = detection[5:]
                classId = np.argmax(scores)
                confidence = scores[classId]
                if confidence > cThreshold:
                    center_x = int(detection[0] * frameWidth)
                    center_y = int(detection[1] * frameHeight)
                    width = int(detection[2] * frameWidth)
                    height = int(detection[3] * frameHeight)
                    left = center_x - width / 2
                    top = center_y - height / 2
                    boxes.append([left, top, width, height])
    return boxes

k=2
def drawBoxes(img, boxes):
    for pos in boxes:
        cv2.rectangle(img, (pos[0]*k, pos[1]*k), (pos[2]*k+pos[0]*k, pos[3]*k+pos[1]*k), (100, 200, 100))


#detector = dlib.get_frontal_face_detector()

old_faces = []
cThreshold = .5

print("Loading model")
#model = cv2.dnn.readNetFromDarknet("/media/extra/models/yolo-face_final.weights")
path = "/media/extra/models/yolo/"
net = cv2.dnn.readNetFromDarknet(path+"yolov2-tiny.cfg", path+"yolov2-tiny.weights")
net.setPreferableTarget(cv2.dnn.DNN_TARGET_OPENCL)
print("Done loading model")


for i in range(100):
    ret, image = camera.read()
blob = cv2.dnn.blobFromImage(image, .1, (416,416))
net.setInput(blob)
outs = net.forward(getOutputsNames(net))

confidences = []
layerNames = net.getLayerNames()
lastLayerId = net.getLayerId(layerNames[-1])
lastLayer = net.getLayer(lastLayerId)



while True:
    ret, image = camera.read()
    if not ret:
        break
    frame = image.copy()
    image = cv2.resize(image, (0, 0), fx=0.5, fy=0.5)

    
    blob = cv2.dnn.blobFromImage(image, .1, (416, 416))
    net.setInput(blob)
    #outs = net.forward(getOutputsNames(net))

    #layerNames = net.getLayerNames()
    #lastLayerId = net.getLayerId(layerNames[-1])
    #lastLayer = net.getLayer(lastLayerId)

    #image = cv2.resize(image, (0, 0), fx=0.5, fy=0.5)

    #faces = detector(image, 1)
    #if len(old_faces) < len(faces):
    #    old_faces = []
    #    for face in faces:
    #        tracker = dlib.correlation_tracker()
    #        tracker.start_track(image, face)
    #        old_faces.append(tracker)
    #else:
    #    for i, tracker in enumerate(old_faces):
    #        quality = tracker.update(image)
    #        if quality > 7:
    #            pos = tracker.get_position()
    #            pos = dlib.rectangle(
    #                int(pos.left()),
    #                int(pos.top()),
    #                int(pos.right()),
    #                int(pos.bottom()),
    #            )
    #            cv2.rectangle(image, (pos.left(), pos.top()), (pos.right(), pos.bottom()),
    #                          (100, 200, 100))
    #        else:
    #            old_faces.pop(i)

                
    boxes = postProcess(image, outs)
    #drawBoxes(frame, boxes)

    cv2.imshow("image", frame)
    key = cv2.waitKey(1) & 0xFF
    if key == ord('q'):
        break

camera.release()
cv2.destroyAllWindows()
