from mtcnn.mtcnn import MTCNN
import numpy as np
import cv2
import openface
import dlib
import time
import pickle
import csv
#import sklearn

#todo: Refactor this code out
openfacedir = "/home/itadmin/build/openface"
datasetdir = "/home/itadmin/Public/datasets"

#openfacedir = "/media/extra/build/openface"
#datasetdir = "/media/extra/datasets"

#https://stackoverflow.com/questions/6893968/how-to-get-the-return-value-from-a-thread-in-python
from functools import wraps
from concurrent.futures import ThreadPoolExecutor
_DEFAULT_POOL = ThreadPoolExecutor(max_workers=3)
def threadpool(f, executor=None):
    @wraps(f)
    def wrap(*args, **kwargs):
        return (executor or _DEFAULT_POOL).submit(f, *args, **kwargs)
    return wrap


class MTCNNDetector:
    def __init__(self):
        self.detector = MTCNN()
        self._tw = 500
        self._th = 500
        #self._tw = 474
        #self._th = 224

    def detect(self, frame):
        #inframe = cv2.resize(frame, (474, 224))
        #inframe=frame
        inframe = cv2.resize(frame, (self._tw, self._th))
        #start_time = time.time()
        detect_results = self.detector.detect_faces(inframe)
        #time_taken = time.time() - start_time
        return detect_results

    def getScaleRatio(self, frame):
        _w = frame.shape[1]
        _h = frame.shape[0]
        return _w/self._tw, _h/self._th

    '''
    Rescales (x,y,w,h) to the original image
    '''
    def rescale(self, frame, box):
        xs,ys = self.getScaleRatio(frame)
        x,y,w,h = box
        return int(x*xs), int(y*ys), int(w*xs), int(h * ys)

    @threadpool
    def detect_async(self, frame):
        return self.detect(frame)

class OFRecognizer:
    def __init__(self):
        recognizedIndices = []
#        with open("./facerec2/featureDir/classifier.pkl", 'rb') as f:
        with open("/tmp/featureDir/classifier.pkl", "rb") as f:
            try:
                (self.le, self.clf) = pickle.load(f, encoding='latin1')  # le - label and clf - classifer
            except:
                (self.le, self.clf) = pickle.load(f)

        with open(datasetdir+'/hudataset/names.csv', 'r') as f:
            reader = csv.reader(f)
            ndata = list(reader)
        self.labels = [el[1] for el in ndata]
        self.labels.insert(0, "Unknown")

    def getLabel(self, index):
        return self.labels[index]


    def infer(self,rep):
        self.threshold = 0.136
        try:
            rep = rep.reshape(1, -1)
        except:
            print ("No Face detected")
            return (None, None)
        predictions = self.clf.predict_proba(rep).ravel()
        maxI = np.argmax(predictions)
        return (self.le.inverse_transform(maxI), predictions[maxI])

    def getRecognized(self):
        return recognizedIndices

    def recognize(self, net, alignedFaces):
        ret = []
        for f in alignedFaces:
            if not (f is None):
                rep = net.forward(f)
                res = self.infer(rep)

                if res[1] < self.threshold: #below confidence level
                    #res[0] = "person-0"
                    res = (None, res[1])
                    #ret.append(None)
                    #continue

                if res[0] != None: #res[0] contains the label
                    lno = int(res[0][7:])
                else:
                    lno = 0
                ret.append((lno, res[1])) #res[1] contains the  confidence
            else:
                ret.append(None)
        return ret

    @threadpool
    def recognize_async(self, net, alignedFaces):
        return self.recognize(net, alignedFaces)




from keras.models import load_model
from common.utils.datasets import get_labels
from common.utils.preprocessor import preprocess_input
from common.utils.inference import apply_offsets


class GenderRec:
    def __init__(self):
        model_path = './common/trained_models/gender_models/simple_CNN.81-0.96.hdf5'
        self.labels = {0:'female', 1:'male'}
        self.classifier = load_model(model_path, compile=False)
        self.target_size = self.classifier.input_shape[1:3]
        self.window = []

    def prepareface(self, rgb_face):
        try:
            face = cv2.resize(rgb_face, (self.target_size))
            return face
        except:
            print("Cannot resize face")
            return None

    def predict(self, faceImage):
        face = np.expand_dims(faceImage, 0)
        face = preprocess_input(face, False)
        gender_prediction = self.classifier.predict(face)
        gender_label_arg = np.argmax(gender_prediction)
        return self.labels[gender_label_arg]

    def predict_faces(self, faces):
        ret = []
        for f in faces:
            if not (f is None):
                f = cv2.cvtColor(f, cv2.COLOR_BGR2RGB)
                prepared_image = self.prepareface(f)
                result = self.predict(prepared_image)
                ret.append(result)
                print(ret)
            else:
                ret.append("Unknown")
        return ret


class EmotionRec:
    def __init__(self):
        model_path = './common/trained_models/emotion_models/fer2013_mini_XCEPTION.102-0.66.hdf5'
        self.labels = get_labels('fer2013')
        self.offsets = (20, 40)
        self.classifier = load_model(model_path, compile=False)
        self.target_size = self.classifier.input_shape[1:3]
        self.window = []

    def prepareface(self, gray_face):
        try:
            face = cv2.resize(gray_face, (self.target_size))
            return face
        except err:
            print("Cannot resize face")
            return None

    def predict(self, faceImage):
        face = preprocess_input(faceImage, False)
        face = np.expand_dims(face, 0)
        face = np.expand_dims(face, -1)
        emotion_label_args = np.argmax(self.classifier.predict(face))
        return self.labels[emotion_label_args]

    def predict_faces(self, faces):
        ret = []
        for f in faces:
            if not (f is None):
                f = cv2.cvtColor(f, cv2.COLOR_BGR2GRAY)
                prepared_image = self.prepareface(f)
                if prepared_image is None:
                    ret.append("")
                    continue

                result = self.predict(prepared_image)
                ret.append(result)
            else:
                ret.append("Unknown")
        return ret




#detector = MTCNNDetector()
dlibDetector = dlib.get_frontal_face_detector()


def draw_detect_boxes(detector, in_img, out_img, detect_results):
    for result in detect_results:
        x,y,w,h = detector.rescale(in_img, result['box'])
        confidence = result['confidence']
        if confidence < 0.85:
            continue
        cv2.rectangle(out_img, (x,y),(x+w,y+h),(128,50,255),2)


        #cv2.putText(out_img, str(confidence), (x+5,y+5), cv2.FONT_HERSHEY_PLAIN, 1.0, (128,76, 250), 2)


class FrameProcessor():
    def __init__(self):
        self.detector = MTCNNDetector()
        self.recognizer = OFRecognizer()
        self.gender_rec = GenderRec()
        self.emotion_rec = EmotionRec()

        self.old_faces = []
        self.k = 1
        self.detect_future = None
        self.detect_results = []

        self.recognize_future = None
        self.recognize_results = []

        self.detect_threshold = 0.85
        self.align = openface.AlignDlib(openfacedir+"/models/dlib/shape_predictor_68_face_landmarks.dat")
        self.ofnet = openface.TorchNeuralNet(openfacedir+"/models/openface/nn4.small2.v1.t7", 96)
        self.facerects = []
        self.aligned_faces = []
        self.recognized_attrs = []
        self.identified_genders = []
        self.identified_emotions = []
        self.update_track = False
        self.imgCache = None
        self.countEmpty = 0
        self.maxEmptyFrames = 50




    def drawBorder(self, img, r):
        cv2.rectangle(img, (r.left(), r.top()), (r.right(), r.bottom()), (255,0,0), 2)

    def process_image(self, in_img):
        #detector = self.detector
        out_img = in_img.copy()
        rgb_img = cv2.cvtColor(in_img, cv2.COLOR_BGR2RGB)
        #gray_img = cv2.cvtColor(in_img, cv2.COLOR_BGR2GRAY)
        old_faces = self.old_faces
        #k = self.k


        if(self.detect_future is None):
            self.imgCache = in_img.copy()
            self.detect_future = self.detector.detect_async(in_img)
        else:
            if self.detect_future.done():
                self.detect_results = self.detect_future.result()
                self.detect_future = None
                #self.update_track = True



#        self.detect_results = self.detector.detect(in_img)
        draw_detect_boxes(self.detector, in_img, out_img, self.detect_results)

        face_rects = self.getDlibRects(in_img)

        if len(face_rects) == 0:
            self.countEmpty += 1
        else:
            self.countEmpty = 0

        if self.countEmpty > self.maxEmptyFrames:
            self.update_track = True

        if(self.recognize_future is None):
            self.recognize_future = self.recognizer.recognize_async(self.ofnet, self.aligned_faces)
        else:
            if self.recognize_future.done():
                self.recognize_results = self.recognize_future.result()
                self.recognize_future = None
                #self.updateTrack = True

        #self.identified_genders = self.gender_rec.predict_faces(self.aligned_faces)
        #self.identified_emotions = self.emotion_rec.predict_faces(self.aligned_faces)

        rec_results = self.recognizer.recognize(self.ofnet, self.aligned_faces)
        rec_results = self.recognize_results
        gender_results = self.identified_genders
        emotion_results = self.identified_emotions
        #print(res)

        #self.face_rects = face_rects
        #print(len(face_rects))

        if len(old_faces) < len(face_rects) :
             self.update_track = False
             self.old_faces = []
             self.aligned_faces = []
             for face in face_rects:
                 self.tracker = dlib.correlation_tracker()
                 self.tracker.start_track(self.imgCache, face)
                 old_faces.append(self.tracker)
                 self.recognized_attrs.append(None)
                 #self.aligned_faces.append(None)
        else:
            self.aligned_faces = []
            for i, self.tracker in enumerate(old_faces):
                footer_text = "Processing.."
                quality = self.tracker.update(in_img)
                if quality > 5:
                    pos = self.tracker.get_position()
                    x,y = (int(pos.left()), int(pos.top()))
                    w,h = (int(pos.right()-x), int(pos.bottom()-y))

                    #cut_img = in_img[y:y+h, x: x+w]

                    #pos = dlib.rectangle( int(pos.left()), int(pos.top()), int(pos.right()) , int(pos.bottom()) )
                    pos = dlib.rectangle(x, y, x+w, y+h)
                    alignedFace = self.align.align(96, in_img, pos, landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)



                    self.aligned_faces.append(alignedFace)
                    self.drawBorder(out_img, pos)
                    #self.recognized_names = rec

                    if i < len(rec_results) and not(rec_results[i] is None):
                        confidence = 0
                        if not(self.recognized_attrs[i] is None):
                            confidence = self.recognized_attrs[i][1]
                        if confidence < rec_results[i][1]: #update only if confidence is greater
                            self.recognized_attrs[i] = (rec_results[i][0], rec_results[i][1])

                    emotion_result = None
                    gender_result = None
                    if i < len(gender_results) and not(gender_results[i] is None):
                        gender_result = gender_results[i]

                    if i < len(emotion_results) and not(emotion_results[i] is None):
                        emotion_result = emotion_results[i]

                    if not (emotion_result is None) or not (gender_result is None):
                        footer_text = "Gender: {} Emotion: {}".format(gender_result, emotion_result)

                    if not(self.recognized_attrs[i] is None):
                        attr = self.recognized_attrs[i]
                        #cv2.putText(out_img, str(self.recognizer.getLabel(attr[0]))+","+str(attr[1]), (x+5,y+5), cv2.FONT_HERSHEY_PLAIN, 1.0, (128,76, 250), 2)


                    cv2.putText(out_img, footer_text, (x+5,y+h), cv2.FONT_HERSHEY_PLAIN, 1.0, (76, 128, 250), 2)

                    #if rec_results[i] is not None:
                    #    cv2.putText(out_img, str(self.recognizer.getLabel(rec_results[i][0]))+","+str(rec_results[i][1]), (x+5,y+5), cv2.FONT_HERSHEY_PLAIN, 1.0, (128,76, 250), 2)
                    #out_img = alignedFace
                else:
                    old_faces.pop(i)
                    self.recognized_attrs.pop(i)

        self.old_faces = old_faces
        return out_img

    def getDlibRects(self, in_img):
        ret = []
        for result in self.detect_results:
            x,y,w,h = self.detector.rescale(in_img, result['box'])
            #x,y,w,h = result['box']
            confidence = result['confidence']
            if confidence < self.detect_threshold:
                continue
            ret.append(dlib.rectangle(x,y,x+w,y+h))
        return dlib.rectangles(ret)
