import numpy as np
#import cv2
#import openface
#import dlib
import os

import pickle
#import pandas as pd

#classifier_model = "./featureDir/classifier.pkl"
#test_image = ""
import openface
import csv
from frapi.multithread import threadpool

openface_path = "/home/itadmin/build/openface"
#openface_path="/media/extra/build/openface"
model_path = "./common/"
class OpenFaceRecognizer:
    def __init__(self, model_path, dnn_path=model_path+"/models/openface/nn4.small2.v1.t7"):
        le, clf = self.load_classifier(model_path)
        self.le = le
        self.clf = clf
        self.net = openface.TorchNeuralNet(dnn_path, imgDim=96, cuda=False)

    def load_names(self, csvFile):
        with open(csvFile, 'r') as f:
            reader = csv.reader(f)
            ndata = list(reader)
        self.labels = [el[1] for el in ndata]
        self.labels.insert(0, "Unknown")

    def personToName(self, person_id):
        try:
            lno = int(person_id[7:])
        except:
            lno = 0
        print(person_id)
        return self.labels[lno]


    def load_classifier(self, model):
        with open(model, 'rb') as f:
            try:
                (le, clf) = pickle.load(f, encoding='latin1')  # le - label and clf - classifer
            except:
                (le, clf) = pickle.load(f)
        return (le, clf)

    def getEmbedding(self, alignedFace):
        return self.net.forward(alignedFace)

    def predict(self, alignedFace):
        rep = self.getEmbedding(alignedFace)
        result = self.infer(rep)
        #if result[1] < 0.5:
        #    result = (b'Unknown', result[1])
        return result

    def predict_multi(self, alignedFaces):
        results = []
        for face in alignedFaces:
            if face is None:
                results.append((b'Cannot process', 0))
            else:
                results.append(self.predict(face))
        return results


    def loadEmbeddings(self, path, single_embedding = True):
        '''
        Load embeddings from an LFW like directory path.
        Make sure the files are aligned
        '''
        import cv2
        es= {}
        #onlyfiles = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))
        onlydir = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]
        max_class = len(onlydir)
        c_class = 0
        for d in onlydir:
            samples = []
            class_path = path+"/"+d
            imgfile_name = [f for f in os.listdir(class_path) if os.path.isfile(os.path.join(class_path, f))]
            print("Loading for {}/{}".format(c_class, max_class))
            c_class += 1
            for file_name in imgfile_name:
                img_path = class_path+"/"+file_name
                img = cv2.imread(img_path)
                samples.append(self.getEmbedding(img))
                if single_embedding == True:
                    break
            es[d] = samples

        #for i in dir:

        return es

    @threadpool
    def predict_multi_async(self, alignedFaces):
        return self.predict_multi(alignedFaces)

    @threadpool
    def predict_async(self, alignedFace):
        return self.predict(alignedFace)

    def infer(self, rep):
        le, clf = self.le, self.clf
        try:
            rep = rep.reshape(1, -1)
        except:
            print ("No Face detected")
            return (None, None)
        predictions = clf.predict_proba(rep).ravel()
        maxI = np.argmax(predictions)
        return (le.inverse_transform(maxI), predictions[maxI])

    #def train(workDir = "./media/extra/datasets/"):
#    print("Loading embeddings")
#    fname = "{}./labels.csv".format(workDir)
#    labels = pd.read_csv(fname, header=None).as_matrix()
