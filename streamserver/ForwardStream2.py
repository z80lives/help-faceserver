import base64
import cv2
import numpy as np
import io
import tornado.web
import tornado.websocket
from tornado.websocket import websocket_connect
from tornado.ioloop import PeriodicCallback
from PIL import Image

# class CVImg():
#     def __init__(self):        
#         self.img = np.zeros((480, 640, 3), np.uint8)

#     def getImage(self):
#         return self.img
#     def setImage(self, img):
#         self.img = img
    
# cvimg = CVImg()

class WebSocket(tornado.websocket.WebSocketHandler):
    def on_open(self):
        print("Starting out socket")
        
    def on_close(self):
        pass
        #self.inws.close()
        
    def on_message(self, message):
        if message == "read_camera":
            print("Reading from in stream")
            self.camera_loop = PeriodicCallback(self.loop, 10)
            self.camera_loop.start()
        else:
            print("Unsupported function: "+message)
            
    def setSharedVar(self, sv):
        self.shared_vars = sv[0]

    def loop(self):
        print("Preparing image")
        sio = io.BytesIO()
        #img = cvimg.getImage()
        #img = sv.shared_vars[0]

        #img = Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
        img = np.zeros((480, 640, 3), np.uint8)
        cv2_im = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

        img = Image.fromarray(cv2_im)        
        img.save(sio, "JPEG")
        
        try:
            self.write_message(base64.b64encode(sio.getvalue()))
        except tornado.websocket.WebSocketClosedError:
            print("Error occured")
            self.camera_loop.stop()

    def check_origin(self, origin):
        return True

class CamInSocket(tornado.websocket.WebSocketHandler):
    def on_message(self, message):
        print("Message")
    
    def on_close():
        print("Camera Feed socket closed") 
