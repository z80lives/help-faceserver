const { lstatSync, readdirSync, existsSync, mkdirSync, writeFileSync, readFileSync } = require('fs');
const { join } = require('path');

const isDirectory = source => lstatSync(source).isDirectory();
const notDirectory = source => !lstatSync(source).isDirectory();
const getDirectories = source =>
      readdirSync(source).map(name => join(source, name)).filter(isDirectory);
const getFiles = source =>
      readdirSync(source).map(name => join(source, name)).filter(notDirectory);

const mkdir = dir =>
      {
	  if (!existsSync(dir)){
	      mkdirSync(dir);
	      return true;
	  }
	  return false;
      };

let getExt = f => {
    var it = f.split('.');
    return it[it.length-1];
};

const saveBase64 = (path, filename, data) => {   
    var b64data = null;
    let ext = getExt(filename);
    if(ext == "jpg" || ext == "jpeg")
	b64data = data.replace(/^data:image\/jpeg;base64,/, "");
    else if(ext == "png")
	b64data = data.replace(/^data:image\/png;base64,/, "");
    else
	throw "Sorry, cannot determine extension.";

      
    console.log("Saving "+path+"/"+filename);
    writeFileSync(path+"/"+filename, b64data, 'base64', function(err) {
	console.log(err);
    });
};


const loadFile =  (path) =>
      readFileSync(path);


const utils = {
    isDirectory: isDirectory,
    getDirectories: getDirectories,
    getFiles: getFiles,
    saveBase64: saveBase64,
    mkdir: mkdir,
    loadFile: loadFile
};

module.exports = utils;
