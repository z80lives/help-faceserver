const serverConfig =  require('../../config/server.json');
const datasetPath = serverConfig.datasets.path;
const sampledir = datasetPath+"sample/";
var Stream = require('../models/Stream.js');
var Camera = require('../models/Camera.js');

const spawn = require("child_process").spawn;
var python_shell = require('python-shell');

const stopDefaultStream = (callback) => {
    let camera = "camerapi";
      Stream.findOne({camera: camera}, (err, stream) =>{
	  if(err){
	      callback(err);
	  }
		
	  if (stream){
	      console.log("Killing stream "+stream.camera + " with pid "+stream.pid);
	     
	      try{
		  process.kill(stream.pid);
	      }catch(ex){
		  stream.remove();
		  callback("Process does not exist");
		  return;
	      }
	      stream.remove();
	      callback(null);
	  }
      });
};


const startDefaultStream = (callback) => {
    let camera = "camerapi";
    Stream.findOne({camera: camera}, (err, stream) =>{
	if(stream){
	    try{
		process.kill(stream.pid);
	    }catch(ex){
		callback("Process not found");
	    }
	    stream.remove();
	}
		
	
	Camera.findOne({name: camera}, (err, cam) => {
	    if(err){
		callback(err);
	    }else{

		if(cam){		   
		    const spawn = require("child_process").spawn;
		    const pythonProcess = spawn('python3',["streamserver/mjpg_forward_server.py", cam.ip]
		    );
		    let streamData = {
			camera: camera,
			pid: pythonProcess.pid,
			port: 4001
		    };
		    Stream.create(streamData, (err) =>{
			if(err){
			    callback("Cannot create stream");
			    console.log(err);
			}
			console.log("Done");
			callback(null);
		    });
		}else{
		    callback("Cannot start stream");
		}
	    }
	});
    });   
};


/**
 * PreConditions: 
 *  - streamConfig existes as Stream Object in database
 *  - cameraConfig existes as Camera Object in database
 **/ 
const startStream = (streamConfig, cameraConfig, callback) => {
    if(streamConfig.active ){
	callback("Cannot start, stream service already active");
	return;
    }

    //preprocess meta data inside string
    var source = cameraConfig.ip;    
    let replace = source.match(/\${(.*)}/);
    let toReplace = null;
    
    if(replace){
	toReplace = replace[0];
	replace = replace[1];
    }
    if(replace == "sample")
	replace = sampledir;
    source = source.replace(toReplace, replace);
    source = source.replace(/([^:]\/)\/+/g, "$1"); //remove double slash


    //load frame_processor first
	
    
    if(cameraConfig){
	
	let params =   [source, ...streamConfig.frameProcessor.split(".")];
	
	let args = ["stream_server2.py", ...params];
	let cwd = __dirname+"/../../streamserver/";

	/*
	var options = {
	    pythonPath: serverConfig.python.path,
	    mode: 'text',
	    scriptPath: cwd,	
	    args: params
	};

	
	const ps = new python_shell("stream_server2.py", options);
	
        ps.on('message', (msg) => {
	    console.log(msg);
	});

	ps.end((err, code, signal) => {
	    console.log("SORRY");
	    console.log(err);
	    console.log(code);
	    console.log(signal);
	    console.log("AOK");	    
	});


	callback(null); */

	let fs = require('fs');
	var out = fs.openSync('/tmp/console.log', 'a');
	var err = fs.openSync('/tmp/error.log', 'a');
	//var out = process.stdout;
	//var err = process.stdout;

	
	const pythonProcess = spawn('python3', args, {cwd:__dirname+"/../../streamserver/", detached: true, stdio: [ 'ignore', out, err ] });

	streamConfig.pid = pythonProcess.pid;
	streamConfig.active = true;


	console.log("Active pid = "+pythonProcess.pid);
	//.stdout.pipe(process.stdout);
	
	pythonProcess.on('close', function(code) {
	    console.log("Closing with code "+code);
	});
	pythonProcess.unref();
	
	callback(null, streamConfig);
	//console.log(args);
	/*streamConfig.save( (err) =>{
	    if(err)
		callback(err);
	    callback(null);
	    //return;
	});*/
	
	/*let streamData = {
	    camera: camera,
	    pid: pythonProcess.pid,
	    port: 4001
	};*/

	/*
	Stream.create(streamData, (err) =>{
	    if(err){
		callback("Cannot create stream");
		console.log(err);
	    }
	    console.log("Done");
	    callback(null);
	});
    }else{
	callback("Cannot start stream");
    }*/
    }else{
	callback("Camera config not received");
	//return;
    }
};


const stopStream = (stream, callback) => {
    if (stream){
	console.log("Killing stream "+stream.camera + " with pid "+stream.pid);
	
	var errMsg = null;
	try{
	    process.kill(parseInt(stream.pid));
	}catch(ex){
	    errMsg=  "Process does not exists";
	    console.log(ex);
	}

	stream.pid = undefined;
	stream.active = false;

	stream.save( err => {
	    if(err)
		callback(err);
	    callback(errMsg);
	});
    }
};


const StreamController = {
    fetchAllStreams: (callback) => {
	Stream.find({}, (err, s) => {
	    if(err){
		console.log(err);		
	    }
	    if(s)
		callback(s);
	});
    },
    startDefault: startDefaultStream,
    stopDefault: stopDefaultStream,
    startStream: startStream,
    stopStream: stopStream
};

module.exports = StreamController;

