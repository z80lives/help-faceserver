const serverConfig =  require('../../config/server.json');
const utils = require('./utils');
const rimraf = require('rimraf');

var fs = require('fs');
var csv = require('csv-parse/lib/sync');

var config = serverConfig.datasets;
config.path = config.path.replace(/\/$/, "")+"/" ;//fix misplaced trailing slash

var sizeOf = require('image-size');

const loadMetadata =  (dataset) =>{
    console.log(config.path+dataset+"/meta.json");
    return JSON.parse(fs.readFileSync(config.path+dataset+"/meta.json",'utf-8'));
}
const loadImageFile =  (dataset, path) =>
      fs.readFileSync(config.path+dataset+"/"+path);

//const removePath = (e,p) => (c=e.substr(p.length))[0]=='/'?c.substr(1):c;
const removeDatasetPath = (e)=> (c=e.substr(config.path.length-1))[0]=='/'?c.substr(1):c;
//const removeDatasetPath = e => removePath(e,config.path.length);

//config.path = removeDatasetPath(config.path);//fix misplaced trailing slash

//Returns all the files in the given directory
const getFiles = (dataset, meta, extra=[]) => {
    var path = [config.path, dataset, meta.info.directory, ...extra].join('/');
    var files= utils.getFiles(path).map((e)=> (c=e.substr(path.length-1))[0]=='/'?c.substr(1):c);
    var ignore = meta.info.ignore;
    if(ignore){ //remove the files in ignore list
	files = files.filter( f => !ignore.map(i => !f.match(i) ).includes(false));
    }
    return files;
};

const readFileToString = (dataset, labelfile) => {
    var path = [config.path, dataset, labelfile].join('/');
    var buffer = fs.readFileSync(path, "utf-8");
    return buffer;
};

/**
 * converts array pairs like [ ['1', 'a'], ['2', 'b'] ] to
 * a json map like {'1': 'a', '2': 'b'}
**/
const arrPairToMap = (arr) => {
    var ret = {};
    for(var i=0; i<arr.length; i++){
	ret[arr[i][0]] = arr[i][1];
    }
    return ret;
};


const getDirectories = (dataset, meta) => {    
    var path = [config.path, dataset, meta.info.directory].join('/');
    var dirs= utils.getDirectories(path).map((e)=> (c=e.substr(path.length-1))[0]=='/'?c.substr(1):c);

    //I am removing this because, it is not likely for us to ignore the directories
    /** 
    var ignore = meta.info.ignore;
    if(ignore){
	files = files.filter( f => !ignore.map(i => !f.match(i) ).includes(false));
    }*/
    return dirs;
};


/**
 * Do not use this unless for debugging.
 * Does not scale well for larger datasets.
 * Store count values in the database.
 **/ 
const countTotalData = (dataset, opts={}) => {
    var meta = loadMetadata(dataset);
    let classes = [];
    if(meta.info.method == "combined"){
	if(meta.info.labels.src == "filename"){
	    var files = getFiles(dataset, meta);
	    
	}
    }else if(meta.info.method == "lfw"){	
 	classes = getDirectories(dataset, meta);
	//var files =getFiles(dataset, meta).filter(m => m.match(regex) == className);
	var ret =  classes.map( cl => getFiles(dataset, meta, [cl]).length );
	//.reduce( (a,b) => a+ b); //Throwing range error

	let count = 0;
	for(let i=0; i < ret.length; i++){
	    count += ret[i];
	}
	return {count:count};

	//return classes;
    }else	
	return null;

};

const listLabels = (dataset, opts={}) => {
    var meta = loadMetadata(dataset);
    var labels = [];
    var table_mode = false;
    var limit = opts.limit || 100;
    var begin = opts.start || 0;
	if(meta.info.method == "combined"){
	    if(meta.info.labels.src == "filename"){
		var files = getFiles(dataset, meta);		
		var re = new RegExp(meta.info.labels.regexp);
		labels = files.map( e => (res=e.match(re))!=null?res[0]:res).filter( (v,i,s) => s.indexOf(v)==i);
	    }else
		throw "Invalid labelling method given in the meta file :"+meta.info.labe.src;
	}else if(meta.info.method == "lfw"){
	    if(opts.hasOwnProperty('table') && opts.table)
		table_mode = true;
	    
	    if (opts.hasOwnProperty('fileLabel') == false && meta.info.labels && meta.info.labels.hasOwnProperty("labelfile") ){

		var labelfile = meta.info.labels.labelfile;
 	 	var buffer = readFileToString(dataset, labelfile);
		var records = csv(buffer);

		//labels = records.map(e => e[0]);
		labels = getDirectories(dataset, meta).sort();
		
		if(table_mode){
		    //Read labels from csv file and return
		    var recMap = {};
		    records.forEach(e => recMap[e[0]]= e[1]);		    
		    labels = labels.map(e => ({'id': e, 'name': (e in recMap)?recMap[e]:e})
				       );
		}
		    
	    }else{
 	 	labels = getDirectories(dataset, meta).sort();
		if(table_mode){
		    labels=labels.map(v => ({'id': v, 'name': v}));
		}
	    }
	    
	} else {
	    throw "Method "+meta.info.method+" not supported";
	}

    return labels.slice(begin, begin+limit);

};


const showClass = (dataset, className, opts={}) => {
    var meta = loadMetadata(dataset);
    
    if(meta.info.method == "combined"){
	var regex = new RegExp(meta.info.labels.regexp);
	var files =getFiles(dataset, meta).filter(m => m.match(regex) == className);

	return  {
	    'label': className,
	    'files': files,
	    'count': files.length
	};
    }
    else if(meta.info.method == "lfw"){
	var ret = {
	    'label': className
	};
	
	if (meta.info.labels && meta.info.labels.hasOwnProperty("labelfile")){
	    var labelfile = meta.info.labels.labelfile;
	    var buffer = readFileToString(dataset, labelfile);
	    var records = csv(buffer);	   
	    var labelMap = arrPairToMap(records);	    
	    ret['outputLabel'] = labelMap[className];
	}
	let files = getFiles(dataset, meta, [className]);
	ret['count'] = files.length;
	ret['files'] = files;
	
	return ret;
    }
    else{
	
	throw "The API calls does not support '"+meta.info.method+"' extraction method yet.";
    }
};

const createClass = (dataset, className, opts={}) => {
    var meta = loadMetadata(dataset);
    var err=null;
         
    if(meta.info.method == "lfw"){
	if(meta.access && meta.access=="crud"){
	    return {success: utils.mkdir([config.path, dataset, className].join("/") )};
	}else{
	    err="Sorry. Dataset does not have write access.";
	}
    }else{
	err="Sorry, only LFW datastructure supported.";
    }


    return {msg: err};
};

const createImage = (dataset, className, body, opts={}) => {
    var meta = loadMetadata(dataset);
    var filename = body.filename;
    var base64Data = body.img;
	   
    if(meta.info.method == "lfw"){
	utils.saveBase64([config.path, dataset, className].join("/"), filename, base64Data);
	return {msg: "File saved."};
    }else{
	return {msg: "Sorry. message", error: true};
    }
};


const showImage = (dataset, className, imageId, opts={}) => {
    var meta = loadMetadata(dataset);
    
    var retrieveMethod = opts.hasOwnProperty("type")? opts.type: null;
    var imgOnly = opts.hasOwnProperty("imgOnly")? true: false;
    var noImg = opts.hasOwnProperty("noImg")? true: false;
    
    try{

	var filename = "";
	if (retrieveMethod == "filename")
	    filename = imageId;
	else
	    if(meta.info.method == "lfw"){
		files = getFiles(dataset, meta, [className]);
		filename = files[imageId];
	    }
	    
	var data = loadImageFile(dataset, className+"/"+filename);
	var dimensions = sizeOf([config.path, dataset,className, filename].join('/'));	
    }catch(err){
	throw "Operation failed: "+err+", "+className+"/"+filename;
	
    }

    var ret = {'type': retrieveMethod};
    ret.dimensions = dimensions;

    
    if(!noImg){
	ret.data = data;
	ret.data = new Buffer(data).toString('base64');
    }
        
    if (imgOnly)
	return data;
    else
	return ret;
};

function createDatasets(params, callback){
    //Step-1: Create a directory
    var dirname = params.dir_name;
    var create = utils.mkdir(config.path+dirname);
    if(!create)
	callback(412, "Sorry cannot create, '"+dirname+"' already exists.");
    
    //Step-2: Create meta.json file
    var metajson = Object.assign({}, params);
    delete metajson.dir_name;        
    var {website, tags} = params;    
    website = !website? "":website;
    tags = !tags?[]:tags;
    metajson = {...metajson, website, tags, "access": "crud"};
    
    fs.writeFile(config.path+dirname+"/meta.json",
		 JSON.stringify(metajson),
		 'utf8',
		 ()=>callback(200, "Successfully created")
		);       

};

function deleteDataset(name, callback){
    
    try{
	const md = loadMetadata(name);
	if(md.access && md.access=="crud")
	    rimraf(config.path+name, ()=>callback(200, "Successfully deleted " ));
	else
	    callback(402, "Operation not permitted");
    }catch(ex){
	callback(400, "Possible, invalid request received. Cannot load metadata for for "+name+". Check whether the directory exists.");
    }
};

const deleteImage = (dataset, cname, imgname, callback) => {
    try{
	const md = loadMetadata(dataset);
	if(md.access && md.access=="crud"){
	    let fname = [config.path+dataset, cname, imgname].join("/");
	    if(fs.existsSync(fname)){
		fs.unlink(fname, ()=> callback("Sucess"));
	    }else{
		callback(null);
	    }
	}else{
	    callback(null);
	}
    }catch(ex){
	callback(400, "Possible, invalid request received. Cannot load metadata for for "+dataset+". Check whether the directory exists.");
    }
};

const deleteClass = (dataset, cname, callback) =>{
    try{
	const md = loadMetadata(dataset);
	if(md.access && md.access=="crud")
	    rimraf([config.path+dataset, cname].join("/"), callback);
    }catch(ex){
	callback(400, "Possible, invalid request received. Cannot load metadata for for "+dataset+". Check whether the directory exists.");
    }
};

const datasets = {
    config: config,
    listAll: () => {
	var files = [];
	files = utils.getDirectories(config.path)
	    .map( removeDatasetPath );
	return files;
    },
    loadMetadata: loadMetadata,
    listLabels: listLabels,
    showClass: showClass,
    createClass: createClass,
    showImage: showImage,
    countTotalData: countTotalData,
    createDataset: createDatasets,
    createImage: createImage,
    deleteDataset: deleteDataset,
    deleteImage: deleteImage,
    deleteClass: deleteClass
};

module.exports = datasets;
