var Camera = require('../models/Camera.js');

const getAllCameras = (callback) => {
      Camera.find({}, callback);
};


const CameraController = {
    getAllCameras: getAllCameras,
    findCameraById: (id, callback) => Camera.findById(id, callback)
};

module.exports = CameraController;

