var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var moment = require('moment');
var config = require("../../config/server.json");

function validatePassword(hash, pass){
    return bcrypt.compareSync(pass, hash);
}

function createToken(user_id, level){
    var expire = moment().add('hours', 3).unix();

    return jwt.sign(
	{
	    userId: user_id,
	    accessLevel: level
	},
	config.security.key,
	{
	    expiresIn: expire
	}
    );
    //return jwt.sign({ foo: 'bar' }, config.security.key);
}

function getUserId(token){
    try{
    var decoded = jwt.verify(token,
			     config.security.key);
	//console.log(decoded);
	return decoded.userId;
    }catch(err){
	console.log("Invalid token received");
    }
    return null;
}

function verifyToken(token, accessLevel){
    try{
    var decoded = jwt.verify(token,
			     config.security.key);
	if(decoded.accessLevel == accessLevel)
	    return true;	
    }catch(err){
	console.log("Invalid token received");
    }
    return false;
}



const security = {
    validatePassword: validatePassword,
    createToken: createToken,
    verifyToken: verifyToken,
    getUserId: getUserId
};

module.exports = security;
