var express = require('express'),
    router = express.Router();

var CameraController = require("../common/camera.js");
var Camera = require('../models/Camera.js');

/**
 * Camera service controller
 **/

/**
 * fetch all active cameras
 */
router.route('/')
    .get((req, res) => {    
    CameraController.getAllCameras( (err,r) =>{					   
	if(r){
		res.json(r);
	}else{
		res.status(501).send({"msg": "Failed to fetch all cameras from the database"});
	}
    });    
})
    .post((req, res) => {
	var camera = new Camera();
	
	camera.name = req.body.name;
	camera.ip = req.body.ip;
	camera.status = "idle";

	camera.save( (err, cam) => {
	    if(err)
		res.send(err);
	    res.json({_id: cam._id, msg: "Camera successfully added!"});
	});
    });


router.route('/:id')
    .get( (req, res) => {
	Camera.findById(req.params.id, (err, obj)=>{
	    if(err)
		res.status(500).send(err);
	    res.send(obj);
	});
    })

    .delete( (req, res) => {
	let camera = Camera.remove({_id: req.params.id}, (err, obj)=>{
	    if(err)
		res.status(500).send(err);
	    let msg = {msg: "Successfully removed"};
	    res.json(msg);
	});

    });




module.exports = router;
