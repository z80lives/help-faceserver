var express = require('express'),
    router = express.Router();

var Collection = require("../models/Collection");

/**
 * CRUD for Collection
 **/
router.route('/')
    .get( (req, res) => {
	Collection.find( (err, collections) => {
	    if(err)
		res.send(err);
	    res.json(collections);
	});
    })
    .post( (req,res)=>{
	var collection = new Collection();
	collection.name = req.body.name;
	
	collection.save( (err) => {
	    if(err)
		res.send(err);
	    res.json({msg: "Collection created successfully"});
	});
	
    });


router.route('/:collection_id')

    .get( (req, res) => {
	Collection.findById(req.params.collection_id, (err, collection) => {
	    if(err)
		res.send(err);
	    res.json(collection);
	});
    })

    .put((req, res) => {
	Collection.findById(req.params.collection_id, (err, collection) => {
	    if(err)
		res.send(err);
	    collection.name = req.body.name;
    collection.save( (err) => {
		if(err)
		    res.send(err);
		res.json({msg: "Collection updated successfully"});
	    });
	});
	
    })

    .delete((req, res) => {
	Collection.remove( {_id: req.params.collection_id },
		       (err) => {
			   if(err)
			       res.send({msg: "Cannot remove collection", err: err});
			   res.json({msg:"Successfully removed!"});
		       });
    });

module.exports = router;
