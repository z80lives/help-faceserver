var express = require('express'),
    router = express.Router();

//var multipart = require('connect-multiparty');
//var multiparty = require('multiparty');
var formidable = require('formidable');
//var multipartMiddleware = multipart();

var fs = require('fs');
var mod_loader = require('../../streamserver/face_modules/mod_loader.js');


// function to encode file data to base64 encoded string
function base64_encode(file) {
    // read binary data
    var bitmap = fs.readFileSync(file);
    // convert binary data to base64 encoded string
    return new Buffer(bitmap).toString('base64');
}

// function to create file from base64 encoded string
function base64_decode(base64str, file) {
    // create buffer object from base64 encoded string, it is important to tell the constructor that the string is base64 encoded
    var bitmap = new Buffer(base64str, 'base64');
    // write buffer to file
    fs.writeFileSync(file, bitmap);
    console.log('******** File created from base64 encoded string ********');
}


/**
 * Basic CRUD for Person
 **/
router.route('/detect', )
    .post( (req,res)=>{
	//modloader.run_module('base', 'detect', );
	//console.log(req.body, req.files);

	var form = new formidable.IncomingForm();
	form.uploadDir = "/tmp/";
	
	
	form.on('progress', function(bytesReceived, bytesExpected) {
	    console.log(bytesReceived);

	});

	form.on('end', function(){
	    console.log("Upload complete");

	});

	//res.json({msg:'Empty request sent!', dbg: req.body});

	form.parse(req, function(err, fields, files) {
	    if (files||fields){
		var inpFile = null;
		if(files.data){
		    inpFile = files.data.path;
		}
		if(Object.keys(fields).length){
		    //const imgname = new Date().getTime().toString();
		    const imgname='test';
		    var filename=Object.keys(fields)[0];
		    var rawData = fields[filename];
		    var base64Data = rawData.replace(/^data:image\/png;base64,/, "");
		    
		    inpFile = '/tmp/'+imgname+'.png';
		    fs.writeFileSync(inpFile,  base64Data, 'base64');
		}

		try{
		    
		    var outFile = inpFile;
		    //var base64str = base64_encode('kitten.jpg');
		    mod_loader.run_module('base', 'detect',{'img_file': inpFile}); 
		    
		    var outData = fs.readFileSync('/tmp/out.png', 'base64');
		    res.json({data: outData});	    
		}catch(err){
		    console.err(err);
		    res.json({msg: "error"});
		}
	    }else{
		res.json({msg:'Empty request sent!'});
	    }
	    
//	    res.json({msg:'received upload:', path: files.data.path});
	    //res.end(util.inspect({fields: fields, files: files}));
	    
	});
	
	
	//res.send("AOK!");
	/*
	// to create some random id or name for your image name
	const imgname = new Date().getTime().toString();
	// to declare some path to store your converted image
	const path = "/tmp/in.png";    


	debugger;	
	const imgData = req.body[Object.keys(req.body)[0]]['png'];

	
	if(typeof imgData === "undefined"){
	    res.json({msg:"Did not receive img data!"});
	    return;
	}

	
	const base64Data = imgData.replace(/^data:([A-Za-z-+/]+);base64,/, '');
	fs.writeFile(path, base64Data, 'base64', (err) => {
	    console.log(err);
	});

	try{
 	    mod_loader.run_module('base', 'detect',{'img_file': "/tmp/in.png"});
	    var outFile = '/tmp/out.png';
	    //var base64str = base64_encode('kitten.jpg');
	    var outData = fs.readFileSync(outFile, 'base64');
	    res.json({filename: "aok1", data: outData});	    
	}catch(err){
	    console.err(err);
	    res.json({msg: "error"});
	}	*/
    });


module.exports = router;
