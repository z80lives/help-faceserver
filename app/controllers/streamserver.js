var express = require('express'),
    router = express.Router();

var Stream = require('../models/Stream.js');
var StreamController = require("../common/stream.js");
var CameraController = require("../common/camera.js");

/**
 * Stream service controller
 **/

/**
 * fetch all active streams
 */
router.get('/', (req, res) => {    
    StreamController.fetchAllStreams( r =>{					 	res.json(r);
    });    
})

    .post('/', (req, res) => {  //saves a stream config in the database
	CameraController.findCameraById(req.body.camera, (err, camera)=>{
	    if(err)
		res.status(401).send({msg: "Sorry, cannot fetch camera.Error occured", error: err});

	    if(camera == null)
		res.status(401).send({msg: "Invalid request. Camera not found"});
	    
	    let required_params = ["port", "name", "description",
				   "camera", "frameProcessor", "settings"
				  ];	    
	    let paramIsNotMissing = required_params
		.map(p => Object.keys(req.body).includes(p) )
		.reduce( (a,b) => a&&b );
	    
	    if(!paramIsNotMissing){
		res.status(501).send({msg: "Required parameters not received. Check the documentation."} );
		return;
	    }

	    let stream = new Stream(req.body);

	    stream.save( (err, stream) => {
		if(err)
		    res.status(428).send({"msg": "Camera not found"});
		let msg =  "Stream successfully created!";
		let response = {...stream._doc, msg};
		res.json(response);
	    });
	   
	});
    });


router.route('/:stream_id')
/**
 * Fetch stream 
 **/
    .get((req, res) => {
	let _id = req.params.stream_id;
	Stream.findById(_id, (err, stream) => {
	    if(err)
		res.status(500).send({'msg': "Cannot fetch stream", error: err});
	    if(stream == null)
		res.status(500).send({'msg': "Stream with the given id was not found"});

	    res.json(stream);
	});
    })
    .delete( (req, res) => {
	let _id = req.params.stream_id;
	Stream.remove({_id: _id}, (err, obj)=>{
	    if(err)
		res.status(500).send({'msg': "Cannot delete", error: err});
	    
	    let msg = {msg: "Successfully removed"};
	    res.json(msg);
	});
    });

/**
 * start stream
 */
router.get('/:stream_id/start', (req, res) => {
    let _id = req.params.stream_id;

    Stream.findById(_id, (err, stream) => {
	if(err)
	    res.status(500).send({'msg': "Cannot fetch stream", error: err});
	if(stream == null)
	    res.status(500).send({'msg': "Stream with the given id was not found"});

	//let camera = CameraController.findCameraById(Stream.
	StreamController.startStream(stream, stream.camera, (err, streamservice) => {
	    if(err){
		res.status(500).send({'msg': "Cannot start stream", error: err});
		return;
	    }
	    streamservice.save( (err) => {
		if(err)
		    res.status(500).send({'msg': "Stream started, but error occured with the database ", error: err});
		else
		    res.json({msg: "Successfully started", stream});
	    });
	});
    }).populate('camera');    
});

/*
 * stop stream
 */
router.get('/:stream_id/stop', (req, res) => {
    let _id = req.params.stream_id;
    Stream.findById(_id, (err, stream) => {
	StreamController.stopStream( stream, (err) => {
	    if(err)
		res.json({msg: err});
	    else
		res.json({msg: "Default stream service stopped"});

	});
    });
    
});


/**
 * Stream service status
 **/
router.get('/:stream_id/status', (req, res) => {
    res.json({status: "AOK!"});
});



module.exports = router;
