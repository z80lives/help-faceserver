var express = require('express'),
    router = express.Router();
var ScreenInfo = require('../models/ScreenInfo');

router.route('/')
    .get( (req, res) => {
	ScreenInfo.find({},  (err, el) => {
	    if(err)
		res.send(err);
	    res.json(el);
	});
    });


router.route('/one')
    .get( (req, res) => {
	ScreenInfo.find( (err, persons) => {
	    if(err)
		res.send(err);
	    res.json(persons);
	});
    });

module.exports = router;
