var express = require('express'),
    datasets = require('../common/datasets'),
    router = express.Router()
    ;

var adminAccess = require("../middleware/auth").adminAccess;

//require("/media/extra/datasets/FLIC/meta.json");
router.route('/')
    .all(adminAccess)
    .get( (req, res) => {
	var get = req.query;
	if(get.hasOwnProperty("format") && get.format == "tree"){
	    var dList = datasets.listAll();
	    var result = dList.map(el => {
		try{
		    var m = datasets.loadMetadata(el);
		    return {name: el, data: m};
		}catch(err){
		}
	    }).filter(el => el != null);
	    res.json(result);
	}else if(get.hasOwnProperty("format") && get.format == "table"){
	    var dList = datasets.listAll();
	    var result = dList.map(el => {
		try{
		    var m = datasets.loadMetadata(el);
		    var method = m.info.method || "undefined";
		    var description= m.description || "-";
		    var website = m.website || "";
		    var tags = m.tags || [];
		    const crud = (m.access&&m.access=="crud")?true:undefined;
		    
		    return {
			uname: el,
			name: m.name,
			description: description,
			method: method,
			website: website,
			tags: tags,
			crud
		    };
		}catch(err){
		}
	    }).filter(el => el != null);
	    res.json(result);
	}else
	    res.json(datasets.listAll());
    })

 /**
  * Create new dataset folder
  **/
    .post(
	(req, res) => {
	// We create only LFW with labels.csv style folder structure by default
	var requiredParams = ['dir_name', 'name', 'type', 'description'];
	requiredParams.forEach( p =>
				{
				    if(!req.body.hasOwnProperty(p))
					res.status(500).send({'msg': "Sorry, parameter "+p+" not received."});
				});	

	const date = new Date();	
	const info = {"method": "lfw", ignore: [".*txt", ".*json"]};
	const params = {date, info, ...req.body};
	
	datasets.createDataset(params, (code, msg)=> res.status(code).send({msg: msg}) );
	});




router.route('/:dataset_name')
    .all(adminAccess)

    .post( (req, res) => {  //Create a new class
	var name = req.params.dataset_name;
	var requiredParams = ['name'];
	requiredParams.forEach( p =>
				{
				    if(!req.body.hasOwnProperty(p))
					res.status(500).send({'msg': "Sorry, parameter "+p+" not received."});
				});	

	var className = req.body.name;
	if(className=="")
	    res.status(500).send({'msg': "Class name cannot be empty."});
	
	res.json(datasets.createClass(name, className));
	
    })

    .get( (req, res) => {
	var list = datasets.listAll();
	var name = req.params.dataset_name;
	var getRequest = req.query;
	
	if(list.indexOf(name) > -1){
	    try{
		datasets.loadMetadata(name); //make sure it is a valid database folder
	    }catch(err){
		//code 500 -> Internal error
		//console.error("Location: "+datasets.config.path+"/"+name+"/meta.json");
		//console.error(err);
		res.status(500).send({'msg': 'Sorry. Cannot load metadata file.', dbg: err});
		return;
	    }
	    try{
		if(getRequest.hasOwnProperty("count")){
		    let ret = datasets.countTotalData(name, opts);
		    if(ret == null)
			res.status(501).send({msg: "Server error"});
		    else
			res.send(ret);
		    return;
		}
		var opts = {};
		if(getRequest.hasOwnProperty('table') && getRequest.table) //client is requesting for a table
		    opts.table = true;
		if(getRequest.hasOwnProperty('fileLabel')) //client is requesting for a table
		    opts.fileLabel = true;

		if(getRequest.hasOwnProperty('limit')) //client is requesting for a table
		    opts.limit = parseInt(getRequest.limit);
		if(getRequest.hasOwnProperty('start')) //client is requesting for a table
		    opts.start = parseInt(getRequest.start);
		
		if(getRequest.hasOwnProperty('imgLink')) //client is requesting for a table
		    opts.imgLink = true;
		
		 //Client is requesting for an array
		//    res.json(datasets.listLabels(name, {fileLabel:true}));
		
		res.json(datasets.listLabels(name, opts));
	    }catch(msg){
		//code 501 means not implemented. Because parse method will fail likely because the feature has been
		//not implemented.
		res.status(501).send({'msg': 'Error parsing the labels: '+ msg});		
		return;
	    }
	}else{
	    //not found
	    //res.status=404;
	    res.send(404).send({'msg': 'Sorry, dataset '+name+' not found.'});
	}
    })
    .delete(
	(req, res) => {
	var name = req.params.dataset_name;
	datasets.deleteDataset(name,
			       (code, msg) => res.status(code).send({msg: msg}) );	
    });

router.route('/:dataset_name/:class_name')
    .all(adminAccess)
    .get( (req, res) =>{
 	var name = req.params.dataset_name;
	var className = req.params.class_name;
	res.json(datasets.showClass(name, className));	
    })

    .post( (req, res) => {  //Add a new image to the class
	var name = req.params.dataset_name;
	var className = req.params.class_name;
	
	var requiredParams = ['img', 'filename'];
	requiredParams.forEach( p =>
				{
				    if(!req.body.hasOwnProperty(p))
					res.status(500).send({'msg': "Sorry, parameter "+p+" not received."});
				});	

	var imageData = req.body;

	let ret = datasets.createImage(name, className, imageData);
	
	if(ret.hasOwnProperty('error'))
	    res.status(500).send(ret);
	else
	    res.json(ret);
	
    })

    .delete( (req, res) => {
 	var name = req.params.dataset_name;
	var className = req.params.class_name;
	
	
	datasets.deleteClass(name, className, ()=> {
	    res.json({msg: "Succesfully deleted"});
	});
    });
;

router.route('/:dataset_name/:class_name/:image')
    .all(adminAccess)
    .get( (req, res) =>{
	var opts = req.query;	
	var name = req.params.dataset_name;
	var className = req.params.class_name;
	var imageId = req.params.image;

	var results = datasets.showImage(name, className, imageId, opts);

	var imgOnly = opts.hasOwnProperty("imgOnly")? true: null;
	var imgDownload = opts.hasOwnProperty("download")? true: false;

	if(imgOnly){
	    if(!imgDownload)
		res.contentType('image/jpg');
	    res.send(results);
	}else
	    res.json(results);
	
    })
    .delete( (req, res) => {
	var name = req.params.dataset_name;
	var className = req.params.class_name;
	var imageId = req.params.image;
	var opts = req.query;
	var isfilename = opts.hasOwnProperty("type")&&opts.type=="filename"? true: false;

	if(!isfilename){
	    res.status(500)
		.send({'msg': "Only filenames allowed. (Set parameter type=filename along with the request"});
	    
	}
	datasets.deleteImage(name, className, imageId, (r) => {
	    if(r == null)
		res.status(500).send({"msg": "Failed to delete"});
	    else{
		res.json({msg:r});
	    }
	});

    });

module.exports = router;
