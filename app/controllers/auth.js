var express = require('express'),
    router = express.Router();
var User = require('../models/User');
var security = require("../common/security");
var adminAccess = require("../middleware/auth").adminAccess;

router.route('/')
    .get(
	(req, res) => {
	 /**
	  * Get's login status
	  * Returns 200 - if logged in
	  **/
	    //console.log(req.hasOwnProperty("cookies") );
	    if(req.hasOwnProperty("cookies")){
		if (typeof req.cookies.token != "undefined"){		    
		    const token =  req.cookies.token;
		    if (security.verifyToken(token, 'admin')){
			console.log("Token accepted");
			res.json({msg: "Already logged in."});
			return;
		    }else{
			res.status(401).send({'msg': "Insufficent previliges"});
		    }
		}
	    }	    
	    res.status(403).send({'msg': "Not logged in."});	    
 	}
    );



router.route('/admin')
    .all(adminAccess)
    .get( (req, res) => {
	res.json({msg: "Logged in as admin."});
    }
);

router.
    get('/logout', (req, res) =>{
	if(res.cookies && res.cookies.token){
	    res.clearCookie("token"); //just in case the client server needs assistance
	}
	res.json({msg: "API Server does not store session information, session must be cleared from the client server/device depending on the implementation."});
    });


router.route('/login').
    post(
	(req, res) => {
	    //console.log(res.cookies);
	    //res.clearCookie("token");
	    if(!req.body.hasOwnProperty("login_id"))
		res.status(401).send({'msg': "Login failed, login_id was not provided"});
	    if(!req.body.hasOwnProperty("password"))
		res.status(401).send({'msg': "Login failed, password was not provided"});
	    var opts = req.query;

	    
	    User.findOne({name: req.body.login_id}, (err, user) =>{
		if(err)
		    res.status(401).send({msg: "User not found"});
		
		if(!user)
		{
		    res.status(401).send({"msg": "Incorrect ID/password given"});
		}


		if(security.validatePassword(user.hash, req.body.password) ){
		    const token = security.createToken(user._id, 'admin');
		    //var dbg = security.verifyToken(token, 'admin');
		    if(opts.hasOwnProperty("setcookie")){
		    	res.cookie('token', token, { maxAge: 900000, httpOnly: true });
		    }
		    //res.cookie('token', token, { maxAge: 900000});
		    res.json({"token": token, "msg":"Successfully authenticated"});
		    
		}else{
		    res.status(401).send({"msg": "Incorrect ID/password given"});

		}
		
	    });
	});

module.exports = router;
