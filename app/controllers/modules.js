var express = require('express'),
    router = express.Router(),
    uuid = require('uuid'),
    rimraf = require('rimraf')
    ;

//var adminAccess = require("../middleware/auth").adminAccess;
var utils = require("../common/utils");
var mod_loader = require('../../streamserver/face_modules/mod_loader.js');

router.route('/')
    .get( (req, res) => {
	var	ret = mod_loader.list_all_modules();
	res.json(ret);
    });

router.route('/:mod_name')
    .get( (req, res) => {
	let mod_name = req.params.mod_name;
	try{
	    let desc = mod_loader.load_description(mod_name);
	    if(desc == null)
	    {
		res.status(500).send("Sorry, problem loading "+mod_name+": Description file cannot be loaded.");
		return;
		
	    }
	    
	    var services = undefined;
	    if(desc.services)
		services = Object.keys(desc.services);

	    var frame_processors = undefined;
	    if(desc.frame_processors)
		frame_processors = desc.frame_processors.map( e => e.id);

	    ;//Object.keys(desc.frame_processors);
	    
	    
	    let name = desc.name;	
	    let out = {name, services, frame_processors};
	    res.json(out);
	    

	}catch(ex){
	    res.status(500).send({msg: "Cannot read "+mod_name+": ",
				  error: ex});
	    return;
	}
	
	return;
    });


router.route('/:mod_name/services')
    .get((req, res) => {
	let mod_name = req.params.mod_name;
	try{
	    let desc = mod_loader.load_description(mod_name);
	    if(desc == null)
	    {
		res.status(500).send("Sorry, problem loading "+mod_name+": Description file cannot be loaded.");
		return;
		
	    }	    
	    var services = null;
	    if(desc.services)
		services = Object.keys(desc.services);
	    if(services == null){
		res.status(500).send({msg: "Sorry. This module does not have a service defined."});
		
	    }
	    res.json(services);
	}catch(ex){
	    res.status(500).send({msg: "Cannot read "+mod_name+": ", error: ex});	
	}
    });


router.route('/:mod_name/frame_processors')
    .get((req, res) => {
	let mod_name = req.params.mod_name;
	try{
	    let desc = mod_loader.load_description(mod_name);
	    if(desc == null)
	    {
		res.status(500).send("Sorry, problem loading "+mod_name+": Description file cannot be loaded.");
		return;
		
	    }	    
	    
	    var frame_processors = null;
	    if(desc.frame_processors)
		frame_processors = desc.frame_processors.map( e => e.id);

	    if(frame_processors == null){
		res.status(500).send({msg: "Sorry. This module does not have a frame processor defined."});
		
	    }
	    res.json(frame_processors);
	}catch(ex){
	    res.status(500).send({msg: "Cannot read "+mod_name+": ", error: ex} );	
	}
    });


router.route('/:mod_name/frame_processors/:fp_name')
    .get( (req, res) => {
	let modname = req.params.mod_name;	
	let fpname = req.params.fp_name;
	res.json(mod_loader.get_frame_processor_info(modname, fpname));
    })
    .post( (req, res) => {
	let modname = req.params.mod_name;	
	let fpname = req.params.fp_name;
	var imgOnly = req.query.hasOwnProperty("imgOnly")? true: false;

	
	if(req.body.img == undefined){
	    res.status(500).send({'msg': "Please send base64 input image in json format with the key 'img'"});
	    return;
	}
	let b64data= req.body.img;
	let workFolder = uuid();
	let path = ["/tmp", "huwf", workFolder].join("/");
	utils.mkdir(["/tmp","huwf"].join("/"));
	utils.mkdir(path);
	
	utils.saveBase64(path, "input.jpg", b64data);
	
	mod_loader.run_frame_processor(path, modname, fpname, (r)=>{	    
	    //rimraf(path, ()=>{}); //clear cache
	    var data = utils.loadFile([path, "output.jpg"].join("/"));
	    //var data = res.data();
	    if(imgOnly){
		res.contentType('image/jpg');
		res.send(data);
		return;
	    }else{
		var buffer = new Buffer(data).toString('base64');
		res.json({msg: r, img: buffer});
	    }
	});
    });
;

module.exports = router;
