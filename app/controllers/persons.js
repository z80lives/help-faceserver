var express = require('express'),
    router = express.Router();
var Person = require('../models/Person');

/**
 * Basic CRUD for Person
 **/
router.route('/')
    .get( (req, res) => {
	Person.find( (err, persons) => {
	    if(err)
		res.send(err);
	    res.json(persons);
	});
    })
    .post( (req,res)=>{
	var person = new Person();
	person.name = req.body.name;
	person.type = req.body.type;
	
	person.save( (err) => {
	    if(err)
		res.send(err);
	    res.json({msg: "Person created successfully"});
	});
	
    });

router.route('/:person_id')

    .get( (req, res) => {
	Person.findById(req.params.person_id, (err, person) => {
	    if(err)
		res.send(err);
	    res.json(person);
	});
    })

    .put((req, res) => {
	Person.findById(req.params.person_id, (err, person) => {
	    if(err)
		res.send(err);
	    person.name = req.body.name;
	    person.type = req.body.type;
	    person.save( (err) => {
		if(err)
		    res.send(err);
		res.json({msg: "Person updated successfully"});
	    });
	});
	
    })

    .delete((req, res) => {
	Person.remove( {_id: req.params.person_id },
		       (err) => {
			   if(err)
			       res.send({msg: "Cannot remove person", err: err});
			   res.json({msg:"Successfully removed!"});
		       });
    });

module.exports = router;
