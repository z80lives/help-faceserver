var express = require('express'),
    router = express.Router();

router.use('/persons',require('./persons'));
router.use('/face/',require('./face'));
router.use('/collections', require('./collections'));
router.use('/datasets', require('./datasets'));
router.use('/auth', require('./auth'));
router.use('/streams', require('./streamserver'));
router.use('/screen', require('./screen'));
router.use('/cameras', require('./cameraserver'));
router.use('/modules', require('./modules'));




/**
 * @api {get} / Retrieves all the available datasets
 */
router.route('/')
    .get(
	(_, res) => 
	    res.send({
		msg: "Web API is ready"
	    })
	
    );


module.exports = router;
