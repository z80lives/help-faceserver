var security = require('../common/security');

const adminAccess = (req, res, next) =>{
    //var token = req.cookies.token;
    console.log("Authenticating admin...");

    //var token = null;
    var token = req.cookies.token ||  (req.query && req.query.access_token) || req.headers['x-access-token'] ;

    //console.log(req.headers['x-access-token']);

    if(token){
	if (security.verifyToken(token, 'admin')){
	    console.log("Token accepted");	    
	    next();
	}
    }else{
	res.status(401).send({'msg': "Insufficent previliges"});
    }
};


module.exports = {
    adminAccess: adminAccess
};
