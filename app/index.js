var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var controllers = require('./controllers');
//var cookieSession = require('cookie-session');
var cookieParser = require("cookie-parser");

var adminAuth = require("./middleware/auth").adminAccess;
var config = require("../config/server.json");
process.title = config.type;

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Access-Token');
    res.header("Access-Control-Allow-Credentials", 'true');

    //intercepts OPTIONS method
    if ('OPTIONS' === req.method) {
      //respond with 200
	res.send(200);
	//next();
    }
    else {
    //move on
      next();
    }
});

app.all('/api/admin/*', adminAuth);

app.use(cookieParser());
 
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.raw());
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));;

//app.use(express.cookieParser());

var port = process.env.PORT || 5000;
//var Person = require('./app/models/Person.js');


var mongoose = require('mongoose');

//Initialize database
try{
    mongoose.connect('mongodb://localhost:27017/hufru');
}catch(ex){
    console.log("Exception occured.");
    console.log(ex);
    process.exit(-1);
}


/**
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});**/

//Load all the routes
app.use('/api', controllers);

//Start listening for incoming traffic
app.listen(port);
console.log("Server is active on port "+port);

//Shutdown
process.on('SIGINT', function() {
    console.log("\n Shutting down WebAPI");
    process.exit(0);
});


module.exports = app;
