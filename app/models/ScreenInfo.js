var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ScreenInfoSchema = new Schema({
    type: String,
    label: String
}, { collection : 'screeninfo' });

module.exports = mongoose.model('ScreenInfo', ScreenInfoSchema);
