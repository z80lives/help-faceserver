var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    sid: String,
    name: String,
    hash: String,
    type: String
});

module.exports = mongoose.model('User', UserSchema);
