var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CameraSchema = new Schema({
    name: String,
    ip: String,
    status: String
});

module.exports = mongoose.model('Camera', CameraSchema);
