var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var StreamSchema = new Schema({
    pid: String,
    port: String,
    name: String,
    description: String,
    active: Boolean,
    camera: {type: Schema.Types.ObjectId, ref:"Camera"},
    frameProcessor: String,
    settings: Object
});

module.exports = mongoose.model('Stream', StreamSchema);
