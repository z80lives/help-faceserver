var bonjour = require('bonjour')();

console.log("Publishing FaceRec server");

bonjour.publish({
    name: 'Facrec API server',
    type: 'HUITFRAPI',
    port: 3002,
    txt: {
	port: 5000
    }
});


process.on('SIGINT', function() {
    console.log("\nExiting broadcast server");
    process.exit(0);
});
