#!/usr/bin/node
var config = require('./config/server.json');
const uuid = require('uuid');
var mongoose = require("mongoose");
mongoose.Promise = global.Promise;
var bcrypt = require('bcrypt');

var argv = process.argv.slice(2);
var args = require("minimist")(argv);


//var db = mongoose.connection;
//db.on('error', console.error.bind(console, 'MongoDB connection error:'));
var User = require('./app/models/User.js');
var Camera = require('./app/models/Camera.js');
var Stream = require('./app/models/Stream.js');
var Person = require('./app/models/Person.js');

var expect_parameters = (args, exp) =>{
	for(let i =0; i<exp.length; i++){
		let m = exp[i];
		if(args.hasOwnProperty(m)==false){
			console.log("Expecting argument "+m);
			process.exit(1);
		}
	}
};

var main = ()=>{
    if(args.hasOwnProperty("create") ){
	if(args.create == "admin"){
            if(args.hasOwnProperty("id")){
		var userId = args.id;
		const uid = uuid();	    
		var password = "welcome@123";
		var hash = bcrypt.hashSync(password, config.security.saltRounds);
		var adminData = {"sid": uid, "name": userId, "type": "admin", "hash": hash};
		console.log("Creating admin with id '"+userId+"' ...");
		console.log(adminData);

		User.create(adminData,  (err) => {
		    if(err)
			console.log(err);
		    console.log("New admin created successfully");
		    console.log("default password is welcome@123");
		    process.exit(0);
		});						
            }else{
		console.log("Expecting user identity");
	    }
	}else if(args.create == "camera"){
		expect_parameters(args, ["name", "ip"]);
		let {ip, name} = args;
		var camData = {name: name, ip: ip, status: "inactive"};
		
		console.log(camData);
		Camera.findOneAndUpdate({name: name},  camData, {upsert:true}, (err, cam) => {
			if(err)
				console.log(err);
			console.log("Done!");
			process.exit(1);
		});
	}else{
            console.log("Expecting admin")
	}

    }else if(args.hasOwnProperty("stream")){
	//expect_parameters(args, ["camera"]);
	let camera = args.stream;
	var killMode = false;

	if(args.hasOwnProperty("kill")){
	    Stream.findOne({camera: camera}, (err, stream) =>{
		if(err){
		    console.log(err);
		    process.exit(1);	
		}
		
		if (stream){
		    console.log("Killing stream "+stream.camera + " with pid "+stream.pid);
		    process.kill(stream.pid);
		    stream.remove()
		    process.exit(0);
		}
	    });	    
	}else{

	    Camera.findOne({name: camera}, (err, cam) => {
		if(err){
		    console.log(err);
		    process.exit(1);	
		}else{
		    if(cam){		   
			const spawn = require("child_process").spawn;
			const pythonProcess = spawn('python3',["streamserver/mjpg_forward_server.py", cam.ip]);
			let streamData = {
			    camera: camera,
			    pid: pythonProcess.pid,
			    port: 4001
			};
			Stream.create(streamData, (err) =>{
			    if(err)
				console.log(err);
			    console.log("Done");
			    process.exit(0);
			});
		    }else{
			console.log("Cannot start stream")
			process.exit(1);
		    }
		}
	    });
	}
			  
			 
			  
		      
    }else if(args.hasOwnProperty("list") && args.list == "admins"){
	console.log("...");    
	User.find((err, users) => {
	    console.log("Listing admins");
	    console.log(users);
	    process.exit(0);    		    
	});


    }else if(args.hasOwnProperty("list") && args.list == "cameras"){
	console.log("...")
	    Camera.find( (err, cameras) => {
		console.log("Listing cameras");
		console.log(cameras);
		process.exit(0);		    
	    });
    }
    else if(args.hasOwnProperty("list") && args.list == "streams"){
	console.log("...")
	    Stream.find( (err, el) => {
		console.log("Listing Streams");
		console.log(el);
		process.exit(0);		    
	    });
    }

    else{
	console.log("Faceserver management tool ");
	console.log("    --create [admin|camera]");
	console.log("    --id : User identity");
	console.log("    --strea : Camera Name");	
	console.log("    --list [admins|cameras|streams]");
	process.exit(0);    	

    }
    //mongoose.connection.close()
    //process.exit(0);

}

mongoose.set('debug', true);
try{
    console.log("Connecting to "+config.db.host);
    const db = mongoose.connect("mongodb://localhost:27017/hufru", { useNewUrlParser: true }).then(()=>{
	main();
    });
}catch(ex){
    console.log("Exception occured.");
    console.log(ex);
    process.exit(-1);
}

//var User = require('./app/models/User.js');
//var Person = require('./app/models/Person.js');


//var ret = Person.find({}, (e, p)=>{console.log(p);});

/*
waitForMongo("", {timeout: 1000 * 60* 2}, function(err) {
    
if(args.hasOwnProperty("create") ){
    if(args.create == "admin"){
        if(args.hasOwnProperty("id")){
            var userId = args.id;
	    const uid = uuid();	    
	    var password = "welcome@123";
	    var hash = bcrypt.hashSync(password, config.security.saltRounds);
	    var adminData = {"sid": uid, "name": userId, "type": "admin", "hash": hash};
	    User.create(adminData,  (err) => {
		if(err)
		    console.log(err);
		console.log("New admin created successfully");
	    });

	    
	    console.log("Creating admin with id '"+userId+"' ...");
	    console.log(adminData);
	    //console.log(newAdmin);

	    var p = new Person({name: "DSAD", type: "dsa"});
	    /*newAdmin.save( (err) => {
		if(err)
		    console.log(err);
		console.log("New admin created successfully");
	    });*//*
	 
	    console.log("default password is welcome@123");
        }else{
	    console.log("Expecting user identity");
	}
    }else{
        console.log("Expecting admin")
    }
}else if(args.hasOwnProperty("list") && args.list == "admins"){
    console.log("...");    
    User.find((err, users) => {
	console.log("Listing users");
	console.log(users);
    });

    Person.find((err, users) => {
	console.log("Listing users");
	console.log(users);
    });
    
}
else{
    console.log("Faceserver management tool ");
    console.log("    --create [admin]");
    console.log("    --id : User identity");
    console.log("    --list [admins]");

}

process.exit(0);

});
*/
