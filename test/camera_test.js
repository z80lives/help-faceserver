let mongoose = require('mongoose');
let Camera = require('../app/models/Camera.js');

let chai = require('chai');
let chaiHttp = require('chai-http');


let server = require('../app/index.js');
let should = chai.should();

chai.use(chaiHttp);

describe("Cameras", () => {
    beforeEach( (done) => {
	Camera.remove({}, (err) => {
	    done();
	});
    });

    /*
     * Test the /GET route
     */
    describe('/GET camera', () => {
	it('it should GET all the cameras', (done) => {
            chai.request(server)
		.get('/api/cameras')
		.end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
		    done();
		});
	});
    });    


    /*
     * Test the /POST route
     */
    describe('/POST camera', () => {	
	it('it should POST a camera ', (done) => {
            let camera = {
		name: "Court Video",
		ip: "${sample}/court.mp4",
		status: ""
            };
	    
            chai.request(server)
		.post('/api/cameras')
		.send(camera)
		.end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('msg').eql('Camera successfully added!');
                    res.body.should.have.property('_id');
		    done();
		});
	});
    });
    

    /**
     * Test the /GET/{id} route
     **/
    describe("/GET/{id} camera", () => {
	it("Should fetch a saved camera on route /api/cameras/<id>", (done) => {

	    let testCam =  new Camera(
		{
		    "name": "Court Video",
		    "ip": "${sample}/court.mp4",
		    "type": "file"
		}
	    );
	    testCam.save( (saveErr, camera) => {
		chai.request(server)
		    .get('/api/cameras/'+camera._id)
		    .end( (err, res) => {
			res.body.should.be.a('object');
			res.should.have.status(200);
			//res.body.should.be.json;
			res.body.should.have.property('_id');
			res.body.should.have.property('name');
			res.body.should.have.property('ip');
			done();
			/*chai.request(server)		    
			  .get("/api/cameras/"+res.body[0]._id).end((error, response) =>{
			  response.should.have.status(200);
			  response.body.should.be.json;
			    done();
			    });*/
		    });
		
	    });

	});
    });

    
    /**
     * Test the /DELETE route
     **/
    describe("/DELETE camera", () => {
	it("Should delete a camera on route /api/cameras/<id>", (done) => {

	    
	    let testCam =  new Camera(
		{
		    "name": "Court Video",
		    "ip": "${sample}/court.mp4",
		    "type": "file"
		}
	    );
	    
	    testCam.save( (saveErr, camera) => {
		chai.request(server)
		    .get('/api/cameras/')
		    .end( (err, res) => {
			chai.request(server)		    
			    .delete("/api/cameras/"+camera._id).end((error, response) =>{
				response.body.should.be.a('object');
				response.should.have.status(200);
				done();
			    });
		    });
		
	    });


	});
    });
});

