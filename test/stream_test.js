let mongoose = require('mongoose');
let Camera = require('../app/models/Camera.js');
let Stream = require('../app/models/Stream.js');

let chai = require('chai');
let chaiHttp = require('chai-http');


let server = require('../app/index.js');
let should = chai.should();

chai.use(chaiHttp);


let cam_id = null;
describe("Streams", () => {
    before( (done) => {
	let cameraData = {
		name: "Court Video",
		ip: "${sample}/court.mp4",
		status: ""
        };
	let camera = new Camera(cameraData);
	camera.save();
	cam_id = camera._id;
	done();
    });

    after( (done) => {
	Camera.remove({_id: cam_id}, (err) => {
	    Stream.remove({}, ()=>{
		done();
	    });
	});

    });


    describe("/GET streams", ()=> {
    it("should get all streams", (done) => {
	chai.request(server)
		.get('/api/streams')
		.end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
		    done();
		});
	
    });
    });


    describe('/POST stream without all parameters', () => {	
	it('it should return HTTP 501  ', (done) => {
	    let stream = {
		name: "Test Stream",
		description: "Testing the video feed",
		camera: cam_id
	    };
	    
            chai.request(server)
		.post('/api/streams')
		.send(stream)
		.end((err, res) => {
                    res.should.have.status(501);
		    done();
		});
	});
    });
    
    
    /*
     * Test the /POST route
     */
    describe('/POST stream with a single frame processor', () => {	
	it('it should POST a stream ', (done) => {
	    let stream = {
		port: 4001,
		name: "Test Stream",
		description: "Testing the video feed",
		camera: cam_id,
		frameProcessor: "funfp.cartoon",
		settings: {}
	    };
	    
            chai.request(server)
		.post('/api/streams')
		.send(stream)
		.end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
		    res.body.should.have.property('_id');
                    res.body.should.have.property('msg').eql('Stream successfully created!');

		    done();
		});
	});
    });
    

    
    /*
     * Test the /POST route with multiple stream
     */
    describe('/POST stream with chained processer pipeline', () => {	
	it('it should POST a stream ', (done) => {
	    let stream = {
		port: 4001,
		name: "Test Stream",
		description: "Testing the video feed",
		camera: cam_id,
		frameProcessor: "[funfp.cartoon]",
		settings: {}
	    };
	    
            chai.request(server)
		.post('/api/streams')
		.send(stream)
		.end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
		    res.body.should.have.property('_id');
                    res.body.should.have.property('msg').eql('Stream successfully created!');        
		    done();
		});
	});
    });   
});

''
